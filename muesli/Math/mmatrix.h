/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/


#pragma once
#ifndef _muesli__mmatrix_h
#define _muesli__mmatrix_h


#include "muesli/Math/realvector.h"
#include <iostream>
#include <ostream>
#include <vector>


//namespace muesli
//{
class itensor;

    class matrix
    {

    public:
                                    matrix();
                                    matrix(const size_t rows, const size_t cols);
                                    matrix(const matrix& m);
                                    matrix(const size_t rows, const size_t cols, double* m);
                                    matrix(const size_t rows, const size_t cols, const realvector& v1, const realvector& v2);
                                    ~matrix();

        matrix&                     operator=(const matrix& m);
        inline          double&     operator()(const size_t i, const size_t j)      {return data[i][j];}
        inline const    double&     operator()(const size_t i, const size_t j) const    {return data[i][j];}
        matrix&                     operator+=(const matrix &v);
        matrix&                     operator-=(const matrix &v);
        realvector                  operator*(const realvector& b) const;
        friend matrix               operator*(const matrix& m1, const matrix& m2);
        friend matrix               operator+(const matrix& m1, const matrix& m2);
        friend matrix               operator-(const matrix& m1, const matrix& m2);
        friend matrix               operator*(double a, const matrix& m);
        friend matrix               operator*(const matrix& m, double a);
        friend matrix               operator*(const matrix& m, const itensor& t);
        friend matrix               operator*(const itensor& t, const matrix& m);


        inline size_t               cols() const {return _cols;}
        inline size_t               rows() const {return _rows;}
        static matrix               identity(size_t n);
        void                        resize(const size_t newrows, const size_t newcols);
        void                        setRandom();
        void                        setZero();
        void                        symmetrize();
        void                        setrow(size_t r, realvector& rv);
        void                        setcol(size_t c, realvector& cv);


        // math operations on matrices
        void                        addTo(const double f, size_t row, size_t col){ (*this)(row,col) += f;}
        void                        chsgn();
        void                        invert();
        double                      determinant() const;
        double                      norm(const char normtype='F');
        void                        round() const;
        void                        transpose();
        matrix                      transposed() const;
        double                      condition();

        // extract data from the matrix
        realvector                  diagonal() const;
        matrix                      block(unsigned rs, unsigned re, unsigned cs, unsigned ce) const;
        double*                     getContiguousData() {return start;}
        const double*               getContiguousData() const {return start;}
        realvector                  row(size_t r);


        // function to LU decompose the matrix, with no pivotting
        bool                        factorLU();


        /* solve Ax=b, overwriting b with x. No pivoting, for general unsymmetric matrices */
        int                         solveFull(realvector& b);
        void                        solveLU(realvector& b);


        /* print the matrix in the stream */
        void                        print(std::ostream& of=std::cout) const;


        // math operations on matrices
        static void                 addBtCB(matrix& m, const matrix& b, const matrix& c, const double f); // m += B^t C B *fact
        static void                 addBtS(realvector& r, const matrix& b, const realvector& s, const double f); // r += B^t S *fact
        static bool                 eigendata(const matrix& K, matrix& evecs, realvector& evaluesR, realvector& evaluesI);
        complexvector               eigenvalues() const;
        static bool                 generalizedEigendata(const matrix& K, const matrix& M, realvector& evals, matrix& evecs);
        static bool                 generalizedEigendata(const matrix& K, const matrix& M, realvector& evals);
        static void                 matrixTimesMatrixTr(const matrix& A, const matrix& B, matrix& C);
        static void                 matrixTrTimesMatrix(const matrix& A, const matrix& B, matrix& C);


    private:
        size_t                      _rows;
        size_t                      _cols;
        double                      *start;             // where the data goes
        double                      **data;
    };

#endif
