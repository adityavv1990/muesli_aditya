/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/



#include "ammaterial.h"
#include <stdio.h>
#include "muesli/Smallstrain/smallstrainlib.h"

// These are the ranges of temperature for changing the material properties in AMconductorMP
#define TRANSITION_SIZE_1 100.0
#define TRANSITION_SIZE_2 10.0

using namespace std;
using namespace muesli;

AMConductorMaterial::AMConductorMaterial(const std::string& name,
                                         const materialProperties& cl)
:
conductorMaterial(name, cl)
{
    kk.resize(6, 0.0);
    muesli::assignValue(cl, "k0", kk[0]);
    muesli::assignValue(cl, "k1", kk[1]);
    muesli::assignValue(cl, "k2", kk[2]);
    muesli::assignValue(cl, "k3", kk[3]);
    muesli::assignValue(cl, "k4", kk[4]);
    muesli::assignValue(cl, "k5", kk[5]);

    cc.resize(10, 0.0);
    muesli::assignValue(cl, "c0", cc[0]);
    muesli::assignValue(cl, "c1", cc[1]);
    muesli::assignValue(cl, "c2", cc[2]);
    muesli::assignValue(cl, "c3", cc[3]);
    muesli::assignValue(cl, "c4", cc[4]);
    muesli::assignValue(cl, "c5", cc[5]);
    muesli::assignValue(cl, "c6", cc[6]);
    muesli::assignValue(cl, "c7", cc[7]);
    muesli::assignValue(cl, "c8", cc[8]);
    muesli::assignValue(cl, "c9", cc[9]);

    rr.resize(9, 0.0);
    muesli::assignValue(cl, "r0", rr[0]);
    muesli::assignValue(cl, "r1", rr[1]);
    muesli::assignValue(cl, "r2", rr[2]);
    muesli::assignValue(cl, "r3", rr[3]);
    muesli::assignValue(cl, "r4", rr[4]);
    muesli::assignValue(cl, "r5", rr[5]);
    muesli::assignValue(cl, "r6", rr[6]);
    muesli::assignValue(cl, "r7", rr[7]);
    muesli::assignValue(cl, "r8", rr[8]);

    muesli::assignValue(cl, "phi", phi);
    muesli::assignValue(cl, "d",  d);
    muesli::assignValue(cl, "tl", tL);
    muesli::assignValue(cl, "ts", tS);
    muesli::assignValue(cl, "tb", tB);
}




AMConductorMaterial::AMConductorMaterial(const std::string& name,
                                         const double rho,
                                         const double phi_,
                                         const double d_,
                                         const double tL_,
                                         const double tS_,
                                         const double tB_,
                                         const double* k,
                                         const double* c,
                                         const double* r)
:
conductorMaterial(name),
kk(),
cc(),
rr(),
tS(tS_),
tL(tL_),
tB(tB_),
phi(phi_),
d(d_)
{
    setDensity(rho);

    for (unsigned a=0; a<6;  a++) kk.push_back(k[a]);
    for (unsigned a=0; a<10; a++) cc.push_back(c[a]);
    for (unsigned a=0; a<9;  a++) rr.push_back(r[a]);
}




AMConductorMaterial::AMConductorMaterial(const std::string& name) :
conductorMaterial(name)
{
    kk.resize(6, 0.0);
    cc.resize(10, 0.0);
    rr.resize(9, 0.0);
}




bool AMConductorMaterial::check() const
{
    bool ret = conductorMaterial::check();

    double static a = 0.0;
    double static b = 0.0;
    double static c = 0.0;
    for (size_t i=0; i<kk.size(); i++)
    {
        a += abs(kk[i]);
    }
    if (a == 0.0)
    {
        ret = false;
        std::cout << "Error in AM material. Zero conductivity.";
    }

    for (size_t i=0; i<cc.size(); i++)
    {
        b += abs(cc[i]);
    }
    if (b == 0.0)
    {
        ret = false;
        std::cout << "Error in AM material. Zero heat capacity.";
    }

    for (size_t i=0; i<rr.size(); i++)
    {
        c += abs(rr[i]);
    }
    if (c == 0.0)
    {
        ret = false;
        std::cout << "Error in AM material. Zero density.";
    }
    return ret;
}




conductorMP* AMConductorMaterial::createMaterialPoint() const
{
    AMConductorMP *mp = new AMConductorMP(*this);
    return mp;
}




double AMConductorMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_CONDUCTIVITY: ret = kk[0];      break;
        default: ret = 0.0;
    }

    return ret;
}




void AMConductorMaterial::print(std::ostream &of) const
{
    of  << "\n   AM conducting material "
        << "\n   Prosity of powder phi : " << phi
        << "\n   Av. dia. of pow. d    : " << d
        << "\n   Solidus temperature   : " << tS
        << "\n   Liquidus temperature  : " << tL
        << "\n   Boiling temperature   : " << tB
        << "\n   Density               : " << density();

    for (size_t a=0; a<kk.size(); a++)
      of  << "\n   Conductivity k" << a << " : " << kk[a];

    for (size_t a=0; a<cc.size(); a++)
      of  << "\n   Heat capacity c" << a << ": " << cc[a];

    for (size_t a=0; a<rr.size(); a++)
      of  << "\n   Density  r" << a << " : " << rr[a];
}




void AMConductorMaterial::setRandom()
{
    conductorMaterial::setRandom();
    phi = muesli::randomUniform(0.1, 0.5);
    d = muesli::randomUniform(20.0e-6, 80.0e-6);
    tS = muesli::randomUniform(1000.0, 1600.0);
    tL = tS + muesli::randomUniform(100.0, 200.0);
    tB = tL + muesli::randomUniform(1000.0, 2000.0);

    for (unsigned a=0; a<6; a++) kk[a] = muesli::randomUniform(-500.0,29.0);
    for (unsigned a=0; a<10; a++) cc[a] = muesli::randomUniform(-5.0e4,5.0e4);
    for (unsigned a=0; a<9; a++) rr[a] = muesli::randomUniform(-10.0,10.0);
}




bool AMConductorMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    conductorMP* p = this->createMaterialPoint();

    isok = p->testImplementation(os);
    delete p;
    return isok;
}




AMConductorMP::AMConductorMP(const AMConductorMaterial& theMaterial)
:
    conductorMP(theMaterial),
    mat(&theMaterial),
    thePhase(powder)
{
}




void AMConductorMP::conductivity(istensor& K) const
{
    double ktheta;
    ktheta = mat->kk[1];
    K = istensor::scaledIdentity(ktheta);
}




void AMConductorMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{
    istensor K;
    conductivity(K);
    tg = na.dot(K*nb);
}




materialState AMConductorMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theVector.push_back(gradT_n);

    return state;
}




materialState AMConductorMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theVector.push_back(gradT_c);

    return state;
}




double AMConductorMP::density() const
{
    double rho;
    rho = mat->rr[0];
    return rho;
}




double AMConductorMP::heatCapacity() const
{
    double rho;
    rho = mat->rr[0];

    double c;
    c = mat->cc[1];
    return c*rho;
}




double AMConductorMP::heatCapacityDerivative() const
{
    double rho;
    double rhoprime;
    rho = mat->rr[0];
    rhoprime = 0.0;

    double c;
    double cprime;
    c = mat->cc[1];
    cprime = 0.0;
    return cprime*rho+rhoprime*c;
}




void AMConductorMP::heatflux(ivector &q) const
{
    istensor K;
    conductivity(K);
    q = -(K*gradT_c);
}




void AMConductorMP::heatfluxDerivative(ivector &qprime) const
{
    double kp;
    kp = 0.0;
    qprime = (-kp)*gradT_c;
}




double AMConductorMP::phase() const
{
    double phase = 0.0;

    if      (thePhase == powder) phase = 0.0;
    else if (thePhase == solid)  phase = 1.0;
    else                         phase = 2.0;

    return phase;
}




void AMConductorMP::setRandom()
{
    conductorMP::setRandom();
    thePhase = (temp_c < mat->tL) ? powder : fluid;
}




double AMConductorMP::thermalEnergy() const
{
    istensor K;
    conductivity(K);
    return 0.5 * gradT_c.dot(K*gradT_c);
}




void AMConductorMP::updateCurrentState(double theTime, double temp, const ivector& gradT)
{
    conductorMP::updateCurrentState(theTime, temp, gradT);

    if (temp_c > mat->tL)
    {
        thePhase = fluid;
    }

    else if (thePhase == fluid && temp_c < mat->tS)
    {
        thePhase = solid;
    }
}




additiveManufacturingMaterial::additiveManufacturingMaterial(const std::string& name,
                                     const materialProperties& cl)
:
material(name, cl),
theSSMaterial(0),
alpha(0.0),
conductivity(0.0), heat_capacity(0.0),
taylor_quinney(1.0), tref(273.0)
{
    if       (cl.find("subtype elastic") != cl.end())      theSSMaterial = new elasticIsotropicMaterial(name, cl);
    else if  (cl.find("subtype plastic") != cl.end())      theSSMaterial = new splasticMaterial(name, cl);
    else if  (cl.find("subtype viscoelastic") != cl.end()) theSSMaterial = new viscoelasticMaterial(name, cl);
    else if  (cl.find("subtype viscoplastic") != cl.end()) theSSMaterial = new viscoplasticMaterial(name, cl);

    muesli::assignValue(cl, "alpha",   alpha);
    muesli::assignValue(cl, "conductivity", conductivity);
    muesli::assignValue(cl, "heatcapacity", heat_capacity);
    muesli::assignValue(cl, "taylor_quinney", taylor_quinney);
    muesli::assignValue(cl, "tref", tref);
    muesli::assignValue(cl, "ts", tS);
    muesli::assignValue(cl, "tl", tL);
}




additiveManufacturingMaterial::~additiveManufacturingMaterial()
{
    if (theSSMaterial != 0) delete(theSSMaterial);
}




bool additiveManufacturingMaterial::check() const
{
    return theSSMaterial->check();
}




additiveManufacturingMP* additiveManufacturingMaterial::createMaterialPoint() const
{
    return new additiveManufacturingMP(*this);
}




double additiveManufacturingMaterial::density() const
{
    return theSSMaterial->density();
}




double additiveManufacturingMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_THERMAL_EXP:    ret = alpha;     break;
        case PR_CONDUCTIVITY:   ret = conductivity; break;
        case PR_THERMAL_CAP:    ret = heat_capacity; break;
        case PR_TAYLOR_QUINNEY: ret = taylor_quinney; break;

        default:
            ret = theSSMaterial->getProperty(p);
    }
    return ret;
}




void additiveManufacturingMaterial::print(std::ostream &of) const
{
    of  << "\n Small strain material for additive manufacturing analysis."
        << "\n Surrogate mechanical model: ";
    theSSMaterial->print(of);

    of  << "\n Reference temperature:                 " << tref
        << "\n Thermal expansion coefficient:         " << alpha
        << "\n Thermal conductivity:                  " << conductivity
        << "\n Heat capacity at const. vol/unit mass: " << heat_capacity
        << "\n Taylor-Quinney parameter:              " << taylor_quinney
        << "\n Solidus temperature tS :               " << tS
        << "\n Liquidus temperatura tL :              " << tL;
}




void additiveManufacturingMaterial::setRandom()
{
    int mattype = discreteUniform(1, 1);
    std::string name = "surrogate small strain material";
    materialProperties mp;

    if (mattype == 0)
        theSSMaterial = new elasticIsotropicMaterial(name, mp);

    else if (mattype == 1)
        theSSMaterial = new splasticMaterial(name, mp);

    else if (mattype == 2)
        theSSMaterial = new viscoelasticMaterial(name, mp);

    else if (mattype == 3)
        theSSMaterial = new viscoplasticMaterial(name, mp);

    else if (mattype == 4)
        theSSMaterial = new GTN_Material(name, mp);

    theSSMaterial->setRandom();
    alpha          = randomUniform(1.0, 2.0);
    tref           = randomUniform(100.0, 400.0);
    heat_capacity  = randomUniform(1.0, 2.0);
    taylor_quinney = randomUniform(0.80, 1.0);
    tS             = randomUniform(100.0, 100.0);
    tL             = tS + randomUniform(50.0, 100.0);
}




bool additiveManufacturingMaterial::test(std::ostream& of)
{
    bool isok = true;
    setRandom();

    additiveManufacturingMP* p = this->createMaterialPoint();

    isok = p->testImplementation(of);
    delete p;

    return isok;
}




double additiveManufacturingMaterial::waveVelocity() const
{
    return theSSMaterial->waveVelocity();
}




additiveManufacturingMP::additiveManufacturingMP(const additiveManufacturingMaterial &m)
:
dtemp_n(0.0), dtemp_c(0.0),
theSSMP(0),
mat(&m),
thePhase(powder)
{
    theSSMP = m.theSSMaterial->createMaterialPoint();
    gradt_n.setZero();
    gradt_c.setZero();
}




additiveManufacturingMP::~additiveManufacturingMP()
{
    if (theSSMP != 0) delete (theSSMP);
}




void additiveManufacturingMP::commitCurrentState()
{
    dtemp_n = dtemp_c;
    gradt_n = gradt_c;
    theSSMP->commitCurrentState();
}




double additiveManufacturingMP::contractWithConductivity(const ivector &v1, const ivector &v2) const
{
    return mat->conductivity * v1.dot(v2);
}




void additiveManufacturingMP::contractWithTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    theSSMP->contractWithTangent(v1, v2, T);
}




void additiveManufacturingMP::contractWithDeviatoricTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    theSSMP->contractWithDeviatoricTangent(v1, v2, T);
}




double additiveManufacturingMP::deviatoricEnergy() const
{
    return theSSMP->deviatoricEnergy();
}




void additiveManufacturingMP::deviatoricStress(istensor& s) const
{
    if (thePhase == solid)
        theSSMP->deviatoricStress(s);
    else
        s.setZero();
}




double additiveManufacturingMP::dissipatedEnergy() const
{
    // mechanical part
    double dissMech  = theSSMP->energyDissipationInStep();

    // heat dissipation -> from coupled part of stress
    const istensor ep_c  = theSSMP->getCurrentPlasticStrain();
    const istensor ep_n  = theSSMP->getConvergedPlasticStrain();
    const istensor eps_c = theSSMP->getCurrentState().theStensor[0];
    const istensor eps_n = theSSMP->getConvergedState().theStensor[0];
    istensor M;
    stressTemperatureTensor(M);

    istensor S = dtemp_c * M;
    double dissHeat = S.dot(ep_c - ep_n);

    return dissMech + dissHeat;
}




// II - Cinverse:Cep for small sThermoMech element
void additiveManufacturingMP::dissipationTangent(itensor4& D) const
{
    theSSMP->dissipationTangent(D);
}




double additiveManufacturingMP::effectiveStoredEnergy() const
{
    const double alpha      = mat->alpha;
    const double cv         = mat->heat_capacity;
    const double kappa      = theSSMP->volumetricStiffness();
    const double tref       = mat->tref;
    const double vol        = theSSMP->getCurrentStrain().trace();

    double       dt_Psistar = dissipatedEnergy();
    double       fmec       = theSSMP->storedEnergy();
    double       fcou       = -3.0 * kappa * alpha * dtemp_c * vol;
    double       fthe       = -0.5 * cv/tref * dtemp_c * dtemp_c;

    return fmec + fcou + fthe + dt_Psistar;
}




double additiveManufacturingMP::entropy() const
{
    const double    cv      = mat->heat_capacity;
    const double    kappa   = theSSMP->volumetricStiffness();
    const double    beta    = 3.0 * kappa * mat->alpha;
    const double    tref    = mat->tref;

    materialState   state_c = theSSMP->getCurrentState();
    const istensor& eps_c   = state_c.theStensor[0];

    return  beta * eps_c.trace() + cv/tref * dtemp_c;
}





double additiveManufacturingMP::freeEnergy() const
{
    const double alpha  = mat->alpha;
    const double cv     = mat->heat_capacity;
    const double kappa  = theSSMP->volumetricStiffness();
    const double tref   = mat->tref;
    const double vol    = theSSMP->getCurrentStrain().trace();

    double       fmec   = theSSMP->storedEnergy();
    double       fcou   = -3.0 * kappa * alpha * dtemp_c * vol;
    double       fthe   = -0.5 * cv/tref * dtemp_c * dtemp_c;

    return fmec + fcou + fthe;
}




istensor additiveManufacturingMP::getConvergedPlasticStrain() const
{
    return theSSMP->getConvergedPlasticStrain();
}




materialState additiveManufacturingMP::getConvergedState() const
{
    materialState mat = theSSMP->getConvergedState();
    mat.theDouble.push_back(dtemp_n);
    mat.theVector.push_back(gradt_n);

    return mat;
}




istensor additiveManufacturingMP::getCurrentPlasticStrain() const
{
    return theSSMP->getCurrentPlasticStrain();
}




materialState additiveManufacturingMP::getCurrentState() const
{
    materialState mat = theSSMP->getCurrentState();
    mat.theDouble.push_back(dtemp_c);
    mat.theVector.push_back(gradt_c);

    return mat;
}




istensor& additiveManufacturingMP::getCurrentStrain()
{
    return theSSMP->getCurrentState().theStensor[0];
}




ivector additiveManufacturingMP::heatflux() const
{
    return -mat->conductivity * gradt_c;
}




double additiveManufacturingMP::plasticSlip() const
{
    return theSSMP->plasticSlip();
}




double additiveManufacturingMP::pressure() const
{
    return theSSMP->pressure();
}




void  additiveManufacturingMP::resetCurrentState()
{
    dtemp_c = dtemp_n;
    gradt_c = gradt_n;
    theSSMP->resetCurrentState();
}




void additiveManufacturingMP::setRandom()
{
    dtemp_c = randomUniform(0.06, 0.13) * mat->tref;
    gradt_c.setRandom();
    theSSMP->setRandom();
}




void additiveManufacturingMP::stress(istensor& sigma) const
{
    const double alpha = mat->alpha;
    const double kappa = theSSMP->volumetricStiffness();

    theSSMP->stress(sigma);

    if (thePhase == solid)
    {
        sigma -= 3.0 * kappa * alpha * (dtemp_c + mat->tref - mat->tS) * istensor::identity();
    }
    else
    {
        sigma *= 0.001;
    }
}




void additiveManufacturingMP::stressTemperatureTensor(istensor& M) const
{
    const double alpha = mat->alpha;
    const double kappa = theSSMP->volumetricStiffness();

    if (thePhase == solid)
    {
        M = -3.0 * kappa * alpha * istensor::identity();
    }
}




void additiveManufacturingMP::tangentElasticities(itensor4& c) const
{
    theSSMP->tangentTensor(c);

    if (thePhase == fluid)
    {
        theSSMP->tangentTensor(c);
        c*= 0.001;
    }
}




double& additiveManufacturingMP::temperature()
{
    return dtemp_c;
}




const double& additiveManufacturingMP::temperature() const
{
    return dtemp_c;
}




bool additiveManufacturingMP::testImplementation(std::ostream& os) const
{
    bool isok = true;

    // set a random update in the material
    istensor eps;
    eps.setZero();

    double temp;
    temp = randomUniform(0.7, 1.3) * mat->tref;

    ivector gradt;
    gradt.setRandom();

    additiveManufacturingMP* ssmp = const_cast<additiveManufacturingMP*>(this);
    ssmp->updateCurrentState(0.0, eps, temp, gradt);
    ssmp->commitCurrentState();

    double tn1 = muesli::randomUniform(0.1,1.0);
    eps.setRandom();
    additiveManufacturingMP& theMP = const_cast<additiveManufacturingMP&>(*this);
    theMP.updateCurrentState(tn1, eps, temp, gradt);

    // programmed tangent of elasticities
    itensor4 tg;
    tangentElasticities(tg);

    // (1)  compare DEnergy with the derivative of Energy
    if (true)
    {
        // programmed stress
        istensor sigma;
        this->stress(sigma);

        // numerical differentiation stress
        istensor numSigma;
        numSigma.setZero();
        const double   inc = 1.0e-3;

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, dtemp_c, gradt_c);
                double Wp1 = effectiveStoredEnergy();

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, dtemp_c, gradt_c);
                double Wp2 = effectiveStoredEnergy();

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, dtemp_c, gradt_c);
                double Wm1 = effectiveStoredEnergy();

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, dtemp_c, gradt_c);
                double Wm2 = effectiveStoredEnergy();

                // fourth order approximation of the derivative
                double der = (-Wp2 + 8.0*Wp1 - 8.0*Wm1 + Wm2)/(12.0*inc);
                numSigma(i,j) = der;
                if (i != j) numSigma(i,j) *= 0.5;

                numSigma(j,i) = numSigma(i,j);

                eps(i,j) = eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, dtemp_c, gradt_c);
            }
        }

        // relative error less than 0.01%
        istensor error = numSigma - sigma;
        isok = (error.norm()/sigma.norm() < 1e-4);

        os << "\n   1. Comparing stress with DWeff.";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error in DWeff computation: " <<  error.norm()/sigma.norm();
            os << "\n      Stress:\n" << sigma;
            os << "\n      Numeric stress:\n" << numSigma;
        }
    }


    // (2) compare tensor c with derivative of stress
    if (true)
    {
        // numeric C
        itensor4 nC;
        nC.setZero();

        // numerical differentiation sigma
        istensor dsigma, sigmap1, sigmap2, sigmam1, sigmam2;
        double   inc = 1.0e-3;

        for (unsigned i=0; i<3; i++)
        {
            for (unsigned j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, dtemp_c, gradt_c);
                stress(sigmap1);

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, dtemp_c, gradt_c);
                stress(sigmap2);

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, dtemp_c, gradt_c);
                stress(sigmam1);

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, dtemp_c, gradt_c);
                stress(sigmam2);

                // fourth order approximation of the derivative
                dsigma = (-sigmap2 + 8.0*sigmap1 - 8.0*sigmam1 + sigmam2)/(12.0*inc);

                if (i != j) dsigma *= 0.5;


                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        nC(k,l,i,j) = dsigma(k,l);
                        nC(k,l,j,i) = dsigma(k,l);
                    }

                eps(i,j) = original;
                eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, dtemp_c, gradt_c);
            }
        }

        // relative error less than 0.01%
        double error = 0.0;
        double norm = 0.0;
        for (unsigned i=0; i<3; i++)
            for (unsigned j=0; j<3; j++)
                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        error += pow(nC(i,j,k,l)-tg(i,j,k,l),2);
                        norm  += pow(tg(i,j,k,l),2);
                    }
        error = sqrt(error);
        norm = sqrt(norm);
        isok = (error/norm < 1e-4);

        os << "\n   2. Comparing tensor C with DStress.";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error in DWeff computation: " <<  error/norm;
        }
    }


    // (3) compare stress and voigt stress


    // (4) compare contract tangent with cijkl
    if (true)
    {
        itensor Tvw;
        ivector v, w;
        v.setRandom();
        w.setRandom();
        contractWithTangent(v, w, Tvw);

        istensor S; S.setRandom();

        itensor nTvw; nTvw.setZero();
        for (unsigned i=0; i<3; i++)
            for (unsigned j=0; j<3; j++)
                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        nTvw(i,k) += tg(i,j,k,l)*v(j)*w(l);
                    }

        // relative error less than 0.01%
        itensor error = Tvw - nTvw;
        isok = (error.norm()/Tvw.norm() < 1e-4);

        os << "\n   4. Comparing contract tangent with C_ijkl v_j w_l.";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error: " << error.norm()/Tvw.norm();
            os << "\n      C{a,b} \n" << Tvw;
            os << "\n      C_ijkl a_j b_l:\n" << nTvw;
        }
    }


    // (5) compare volumetric and deviatoric tangent contraction


    // (6) compare tangent with volumetric+deviatoric


    return isok;
}




double additiveManufacturingMP::phase() const
{
    double phase = 0.0;

    if      (thePhase == powder) phase = 0.0;
    else if (thePhase == solid)  phase = 1.0;
    else                         phase = 2.0;

    return phase;
}




void additiveManufacturingMP::updateCurrentState(const double t, const istensor& strain, const double temp, const ivector& gradt)
{
    dtemp_c  = temp;
    gradt_c  = gradt;
    theSSMP->updateCurrentState(t, strain);

    if (temp > mat->tL)
    {
        thePhase = fluid;
    }

    else if (thePhase == fluid && temp < mat->tS)
    {
        thePhase = solid;
    }
}




double additiveManufacturingMP::volumetricEnergy() const
{
    return theSSMP->volumetricEnergy();
}




double additiveManufacturingMP::volumetricStiffness() const
{
    return theSSMP->volumetricStiffness();
}
