/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/


#include "muesli/muesli.h"
#include <iostream>
#include <ctime>
#include <cstdlib>

#ifdef _WIN32

#define _UNISTD_H    1

/* This is intended as a drop-in replacement for unistd.h on Windows.
 * Please add functionality as needed.
 * https://stackoverflow.com/a/826027/1202830
 */

#include <stdlib.h>
#include <io.h>
//#include <getopt.h> /* getopt at: https://gist.github.com/ashelly/7776712 */
#include <process.h> /* for getpid() and the exec..() family */
#include <direct.h> /* for _getcwd() and _chdir() */

#define srandom srand
#define random rand

 /* Values for the second argument to access.
    These may be OR'd together.  */
#define R_OK    4       /* Test for read permission.  */
#define W_OK    2       /* Test for write permission.  */
//#define   X_OK    1       /* execute permission - unsupported in windows*/
#define F_OK    0       /* Test for existence.  */

#define access _access
#define dup2 _dup2
#define execve _execve

#define ftruncate _chsize
#define unlink _unlink
#define fileno _fileno
#define getcwd _getcwd
#define chdir _chdir
#define isatty _isatty
#define lseek _lseek
/* read, write, and close are NOT being #defined here, because while there are file handle specific versions for Windows, they probably don't work for sockets. You need to look at your app and consider whether to call e.g. closesocket(). */

#ifdef _WIN64
#define ssize_t __int64
#else
#define ssize_t long
#endif

#define STDIN_FILENO 0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2
/* should be in some equivalent to <sys/types.h> */
//typedef __int8            int8_t;
typedef __int16           int16_t;
typedef __int32           int32_t;
typedef __int64           int64_t;
typedef unsigned __int8   uint8_t;
typedef unsigned __int16  uint16_t;
typedef unsigned __int32  uint32_t;
typedef unsigned __int64  uint64_t;

#else
#include <unistd.h>

#endif /* unistd.h  */


int main(int argc, char **argv)
{
    std::ofstream os("testmuesli.log");

    os << "            M U E S L I    T E S T S\n\n";

#ifdef __unix__
    time_t walltime = time(NULL);
    os << " Test done on : " << ctime(&walltime);
    os << " Username     : " << getlogin();

    char host[256];
    if (gethostname( host, 256) == 0)
    {
        os << "\n Hostname     : " << host;
    }
    else
    {
        os << "\n Hostname     : unknown";
    }

    static char noOS[] = "UNKNOWN OS";
    char *op = getenv("OSTYPE");
    if (op == NULL) op = getenv("OS");
    if (op == NULL) op = noOS;
    os << "\n OS           : " << op;

#endif

    bool isok = true;

    std::string st_redMethod = "nr";
    //std::string st_redMethod = "";

    {
        muesli::materialProperties mp;
        std::string name = "elastic material";
        muesli::elasticIsotropicMaterial* m = new muesli::elasticIsotropicMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain isotropic elastic material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        muesli::materialProperties mp;
        std::string name = "anisotropic material";
        muesli::elasticAnisotropicMaterial* m = new muesli::elasticAnisotropicMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain anisotropic elastic material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        muesli::materialProperties mp;
        std::string name = "orthotropic material";
        muesli::elasticOrthotropicMaterial* m = new muesli::elasticOrthotropicMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain orthotropic elastic material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        muesli::materialProperties mp;
        std::string name = "Transversely isotropic material";
        muesli::elasticTransverselyisotropicMaterial* m = new muesli::elasticTransverselyisotropicMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain Transversely isotropic elastic material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        muesli::materialProperties mp;
        std::string name = "visco elastic material";
        muesli::viscoelasticMaterial* m = new muesli::viscoelasticMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain visco elastic material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        muesli::materialProperties mp;
        std::string name = "a plastic material";
        muesli::splasticMaterial* m = new muesli::splasticMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain elasto plastic material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a viscoplastic material";
        muesli::materialProperties mp;
        muesli::viscoplasticMaterial* m = new muesli::viscoplasticMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing smallstrain elasto visco plastic material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    if ((false))
    {
        muesli::materialProperties mp;
        std::string name = "Gurson-Tveergaard-Needleman material";
        muesli::GTN_Material* m = new muesli::GTN_Material(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain Gurson-Tveergaard-Needleman damage material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    if ((false))
    {
        muesli::materialProperties mp;
        std::string name = "Lemaitre-Chaboche material";

        muesli::Lemaitre_Material* m = new muesli::Lemaitre_Material(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain Lemaitre-Chaboche damage material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    if ((false))
    {
        muesli::materialProperties mp;
        std::string name = "Lemaitre-Chaboche material with kinematic hardening";

        muesli::LemKin_Material* m = new muesli::LemKin_Material(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain Lemaitre-Chaboche damage material with kinematic hardening";
        os << "\n----------------------------------------------------";

        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    if ((false))
    {
        muesli::materialProperties mp;
        std::string name = "Gurson material";

        muesli::Gurson_Material* m = new muesli::Gurson_Material(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain Gurson damage material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a small strain thermomechanical material";
        muesli::materialProperties mp;
        muesli::sThermoMechMaterial* m = new muesli::sThermoMechMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain thermomechanical material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a SV-K material";
        muesli::materialProperties mp;
        muesli::svkMaterial* m = new muesli::svkMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing Saint Venant - Kirchhoff material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a neohookean material";
        muesli::materialProperties mp;
        muesli::neohookeanMaterial* m = new muesli::neohookeanMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing Neohookean material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a mooney rivlin material";
        muesli::materialProperties mp;
        muesli::mooneyMaterial* m = new muesli::mooneyMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing Mooney - Rivlin material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "an arruda boyce material";
        muesli::materialProperties mp;
        muesli::arrudaboyceMaterial* m = new muesli::arrudaboyceMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing Arruda - Boyce material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a Yeoh material";
        muesli::materialProperties mp;
        muesli::yeohMaterial* m = new muesli::yeohMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing Yeoh material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a fs elastoplastic material";
        muesli::materialProperties mp;
        muesli::fplasticMaterial* m = new muesli::fplasticMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing finite strain elastoplastic material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a fourier material";
        muesli::fourierMaterial* m = new muesli::fourierMaterial(name);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing conductor material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "an anisotropic material";
        muesli::anisotropicConductorMaterial* m = new muesli::anisotropicConductorMaterial(name);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing anisotropic conductor material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a conductor material for AM simulations";
        muesli::AMConductorMaterial* m = new muesli::AMConductorMaterial(name);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing conductor material for AM simulations";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a newtonian fluid material";
        muesli::newtonianMaterial* m = new muesli::newtonianMaterial(name);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing newtonian material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a finite strain thermomechanical material";
        muesli::materialProperties mp;
        muesli::thermofiniteStrainMaterial* m = new muesli::thermofiniteStrainMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing finite strain, thermomechanical material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    if ((false))
    {
        std::string name = "a thermomechanical Johnson-Cook model material";
        muesli::materialProperties mp;
        muesli::thermoJCMaterial* m = new muesli::thermoJCMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing Johnson-Cook, thermomechanical material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    if ((false))
    {
        std::string name = "a thermomechanical Zerilli-Armstrong model material";
        muesli::materialProperties mp;
        muesli::thermoZAMaterial* m = new muesli::thermoZAMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing Zerilli-Armstrong, thermomechanical material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a finite strain coupled stress/diffusion material";
        muesli::materialProperties mp;
        muesli::fMechMassMaterial* m = new muesli::fMechMassMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing finite strain, stress/diffusion material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a Johnson-Cook finite strain material";
        muesli::materialProperties mp;
        muesli::johnsonCookMaterial* m = new muesli::johnsonCookMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing Johnson-Cook finite strain material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "a Zerilli-Armstrong finite strain material";
        muesli::materialProperties mp;
        muesli::zerilliArmstrongMaterial* m = new muesli::zerilliArmstrongMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing Zerilli-Armstrong finite strain material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        std::string name = "an Arrhenius-type finite strain material";
        muesli::materialProperties mp;
        muesli::arrheniusTypeMaterial* m = new muesli::arrheniusTypeMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing Arrhenius-type finite strain material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    if (false)
    {
        std::string name = "a finite strain, thermo-chemo-mechanical material (Anand 2011 IJSS)";
        muesli::materialProperties mp;
        muesli::AnandIJSS2011Material* m = new muesli::AnandIJSS2011Material(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing finite strain, thermo-chemo-mech material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        // testing linear diffusion material
        std::string name = "linear diffusion of a species in a metal host";
        muesli::materialProperties mp;
        muesli::linearDiffusionMaterial* m = new muesli::linearDiffusionMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing linear diffusion material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        // testing non linear diffusion material
        std::string name = "non linear diffusion of a species in a metal host";
        muesli::materialProperties mp;
        muesli::nonLinearDiffusionMaterial* m = new muesli::nonLinearDiffusionMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing non linear diffusion material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        // testing dissolved hydrogen material
        std::string name = "diffusion of hydrogen in a metal host";
        muesli::materialProperties mp;
        muesli::dissolvedHydrogenMaterial* m = new muesli::dissolvedHydrogenMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing dissolved hydrogen material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        // testing smechmasshyd material (stress - hydrogen diffusion material)
        std::string name = "stress coupled with diffusion of hydrogen in a metal host";
        muesli::materialProperties mp;
        muesli::sMechMassHydMaterial* m = new muesli::sMechMassHydMaterial(name, mp);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing stress - hydrogen diffusion material";
        os << "\n----------------------------------------------------";
        bool mok = m->test(os);
        isok = isok && mok;
        delete m;
    }

    {
        // testing small strain beams (elastic and plastic)
        muesli::materialProperties mp;
        std::string name = "smalstrain reduced material";
        //muesli::splasticMaterial* m = new muesli::splasticMaterial(name, mp);
        muesli::elasticIsotropicMaterial* m = new muesli::elasticIsotropicMaterial(name, mp);

        m->setRandom();

        muesli::smallStrainMP* p = m->createMaterialPoint();
        muesli::sbeamMP* sp = new muesli::sbeamMP(p, st_redMethod);
        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing smallstrain reduced beam plastic material";
        os << "\n----------------------------------------------------";
        bool mok = sp->testImplementation(os);
        isok = isok && mok;
        delete m;
    }

    {
        muesli::materialProperties mp;
        std::string name = "smallstrain reduced material";
        //muesli::splasticMaterial* m = new muesli::splasticMaterial(name, mp);
        muesli::elasticIsotropicMaterial* m = new muesli::elasticIsotropicMaterial(name, mp);

        m->setRandom();

        muesli::smallStrainMP* p = m->createMaterialPoint();
        muesli::sshellMP* sp = new muesli::sshellMP(p, st_redMethod);

        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain reduced shell plastic material";
        os << "\n----------------------------------------------------";
        bool mok = sp->testImplementation(os);
        isok = isok && mok;
        delete m;
    }

    {
        muesli::materialProperties mp;
        std::string name = "smallstrain reduced material";
        //muesli::splasticMaterial* m = new muesli::splasticMaterial(name, mp);
        muesli::elasticIsotropicMaterial* m = new muesli::elasticIsotropicMaterial(name, mp);

        m->setRandom();

        muesli::smallStrainMP* p = m->createMaterialPoint();
        muesli::splaneMP* sp = new muesli::splaneMP(p, st_redMethod);

        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain reduced plane plastic material";
        os << "\n----------------------------------------------------";
        bool mok = sp->testImplementation(os);
        isok = isok && mok;
        delete m;
    }

    {
        // testing small strian beams (elastic and plastic)
        muesli::materialProperties mp;
        std::string name = "smalstrain reduced material";
        muesli::splasticMaterial* m = new muesli::splasticMaterial(name, mp);
        //muesli::elasticIsotropicMaterial* m = new muesli::elasticIsotropicMaterial(name, mp);

        m->setRandom();

        muesli::smallStrainMP* p = m->createMaterialPoint();
        muesli::sbarMP* sp = new muesli::sbarMP(p, st_redMethod);

        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing small strain reduced bar plastic material";
        os << "\n----------------------------------------------------";
        bool mok = sp->testImplementation(os);
        isok = isok && mok;
        delete m;
    }

    {
        muesli::materialProperties mp;
        std::string name = "finitestrain elastic material";
        muesli::neohookeanMaterial* m = new muesli::neohookeanMaterial(name, mp);
        //muesli::fplasticMaterial* m = new muesli::fplasticMaterial(name, mp);

        m->setRandom();

        muesli::finiteStrainMP* p = m->createMaterialPoint();
        muesli::fbeamMP* fp = new muesli::fbeamMP(p, st_redMethod);

        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing finite strain reduced beam plastic material";
        os << "\n----------------------------------------------------";
        bool mok = fp->testImplementation(os);
        isok = isok && mok;
        delete m;
    }

    {
        muesli::materialProperties mp;
        std::string name = "finitestrain elastic material";
        muesli::neohookeanMaterial* m = new muesli::neohookeanMaterial(name, mp);
        //muesli::fplasticMaterial* m = new muesli::fplasticMaterial(name, mp);

        m->setRandom();

        muesli::finiteStrainMP* p = m->createMaterialPoint();
        muesli::fshellMP* fp = new muesli::fshellMP(p, st_redMethod);

        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing finite strain reduced shell plastic material";
        os << "\n----------------------------------------------------";
        bool mok = fp->testImplementation(os);
        isok = isok && mok;
        delete m;
    }

        {
        muesli::materialProperties mp;
        std::string name = "finitestrain elastic material";
        //muesli::neohookeanMaterial* m = new muesli::neohookeanMaterial(name, mp);
        muesli::fplasticMaterial* m = new muesli::fplasticMaterial(name, mp);

        m->setRandom();

        muesli::finiteStrainMP* p = m->createMaterialPoint();
        muesli::fbarMP* fp = new muesli::fbarMP(p, st_redMethod);

        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing finite strain reduced bar plastic material";
        os << "\n----------------------------------------------------";
        bool mok = fp->testImplementation(os);
        isok = isok && mok;
        delete m;
    }

    {
        muesli::materialProperties mp;
        std::string name = "finitestrain elastic material";
        //muesli::neohookeanMaterial* m = new muesli::neohookeanMaterial(name, mp);
        muesli::fplasticMaterial* m = new muesli::fplasticMaterial(name, mp);

        m->setRandom();

        muesli::finiteStrainMP* p = m->createMaterialPoint();
        muesli::fplaneMP* fp = new muesli::fplaneMP(p, st_redMethod);

        os << "\n";
        os << "\n----------------------------------------------------";
        os << "\n Testing finitestrain reduced plane plastic material";
        os << "\n----------------------------------------------------";
        bool mok = fp->testImplementation(os);
        isok = isok && mok;
        delete m;
     }


    return isok;
}
