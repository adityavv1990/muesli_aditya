/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/


#include "muesli/Math/mtensor.h"
#include "muesli/Utils/utils.h"
#define _USE_MATH_DEFINES

#include <cassert>
#include <cmath>
#include <iomanip>
#include <string.h>

#include "sdamage.h"

// special matrices
static double           mul2_help[] = { 1.0,0.0,0.0, 0.0,0.0,0.0,
                                        0.0,1.0,0.0, 0.0,0.0,0.0,
                                        0.0,0.0,1.0, 0.0,0.0,0.0,
                                        0.0,0.0,0.0, 2.0,0.0,0.0,
                                        0.0,0.0,0.0, 0.0,2.0,0.0,
                                        0.0,0.0,0.0, 0.0,0.0,2.0};
const static matrix     II_mul2(6,6,mul2_help); // reduced two-tensor version to multiply off-diagonal tensor entries

static double           s2_help[] = {   1.0,0.0,0.0, 0.0,0.0,0.0,
                                        0.0,1.0,0.0, 0.0,0.0,0.0,
                                        0.0,0.0,1.0, 0.0,0.0,0.0,
                                        0.0,0.0,0.0, 0.5,0.0,0.0,
                                        0.0,0.0,0.0, 0.0,0.5,0.0,
                                        0.0,0.0,0.0, 0.0,0.0,0.5};
const static matrix     II_s2(6,6,s2_help); // reduced two-tensor version of symmetric 4th order identity matrix

static double           x2_help[] = {   1.0,1.0,1.0, 0.0,0.0,0.0,
                                        1.0,1.0,1.0, 0.0,0.0,0.0,
                                        1.0,1.0,1.0, 0.0,0.0,0.0,
                                        0.0,0.0,0.0, 0.0,0.0,0.0,
                                        0.0,0.0,0.0, 0.0,0.0,0.0,
                                        0.0,0.0,0.0, 0.0,0.0,0.0};
const static matrix     IxI_2(6,6,x2_help); // reduced two-tensor version of 4th order matrix of identity matrix dyadic product identity matrix

static double           dev2_help[] = { 2.0/3.0,-1.0/3.0,-1.0/3.0, 0.0,0.0,0.0,
                                        -1.0/3.0,2.0/3.0,-1.0/3.0, 0.0,0.0,0.0,
                                        -1.0/3.0,-1.0/3.0,2.0/3.0, 0.0,0.0,0.0,
                                        0.0,0.0,0.0, 1.0,0.0,0.0,
                                        0.0,0.0,0.0, 0.0,1.0,0.0,
                                        0.0,0.0,0.0, 0.0,0.0,1.0};
const static matrix     II_dev2(6,6,dev2_help); // reduced two-tensor version of the deviatoric matrix in 4-th order

/*
 * splastic.cpp,
 * infinitesimal kinematics plastic element
 * e.m. andres, j.segurado, i. romero, oct 2017
 *
 *
 *  1) Gurson-Tvergaard-Needleman model with isotropic damage with no hardening
 *
 *     The yield function depends on:
 *        - t:        trace of sigma
 *        - sigma_eq: (3/2)^0.5*sigma_dev
 *        - fv:       volume fraction of cavities or porosity
 *
 *     where sigma_dev is the square root of the deviatoric stress tensor : deviatoric stress tensor
 *
 *     We = (1-d)*e_elastic:C_e_elastickappa/2 (theta)^2 + mu * ||e_elastic||^2
 *     Wp = NONE (at this moment)
 *     f(sigma, Y(fv)) = (sigma_eq/yield)^2 +
 *                        + 2*q1*fv*cosh((1/2)*q2*t^2/yield) -
 *                        - 1 - (q1*fv)^2;
 *
 */



#define FVMAXITER   20        // max num of allowed iterations to compute fv from d value
#define FVTOL       1e-10     // residual tolerance to compute fv from d value
#define TOL         1e-8      // residual tolerance for return-to-yield-surface solutions
#define MAXITER     70        // max num of allowed iterations in return-to-yield solutions
#define SQRT3_2     sqrt(1.5)

using namespace muesli;
using namespace std;

/* Implementation of base class: sdamage material and material point */
sdamageMaterial::sdamageMaterial(const std::string& name, const double xE, const double xnu,
                                 const double xrho, const double xY0) :
smallStrainMaterial(name),
E(xE), nu(xnu), bulk(0.0),
cp(0.0), cs(0.0), lambda(0.0), mu(0.0), rho(xrho),
Y0(xY0)
{
    lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
    mu     = E/2.0/(1.0+nu);
    bulk   = lambda + 2.0/3.0 * mu;

    if (rho > 0.0)
    {
        cp = sqrt((lambda+2.0*mu)/rho);
        cs = sqrt(2.0*mu/rho);
    }
}




sdamageMaterial::sdamageMaterial(const std::string& name, const materialProperties& cl):
smallStrainMaterial(name, cl),
E(0.0), nu(0.0), bulk(0.0), cp(0.0),
cs(0.0), lambda(0.0), mu(0.0), rho(0.0), Y0(0.0)
{
    muesli::assignValue(cl, "young",       E);
    muesli::assignValue(cl, "poisson",     nu);
    muesli::assignValue(cl, "lambda",      lambda);
    muesli::assignValue(cl, "mu",          mu);
    muesli::assignValue(cl, "yieldstress", Y0);
    muesli::assignValue(cl, "density", rho);

    std::string mstr;
    muesli::assignValue(cl, "model", mstr);

    if (E*E > 0)
    {
        lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
        mu     = E/2.0/(1.0+nu);
    }
    else
    {
        nu     = lambda / 2.0 / (lambda+mu);
        E      = mu*2.0*(1.0+nu);
    }


    bulk = lambda + 2.0/3.0 * mu;
    if (rho > 0.0)
    {
        cp   = sqrt((lambda+2.0*mu)/rho);
        cs   = sqrt(2.0*mu/rho);
    }
}




void sdamageMaterial::print(std::ostream &of) const
{

    of << "\n   Small strain, elasto-plastic, isotropic damage material ";
    of << std::flush;
}




double sdamageMaterial::waveVelocity() const
{
    return cp;
}




sdamageMP::sdamageMP(const sdamageMaterial &m) :
muesli::smallStrainMP(m),
d_n(0.0), dg_n(0.0), ep_n(0.0,0.0,0.0,0.0,0.0,0.0),
d_c(0.0), dg_c(0.0), ep_c(0.0,0.0,0.0,0.0,0.0,0.0)
{

}




double sdamageMP::pressure() const
{
    istensor sigma;
    stress(sigma);

    return -sigma.trace()/3.0;
}




materialState sdamageMP::getConvergedState() const
{
    materialState state_n = smallStrainMP::getConvergedState();

    state_n.theDouble.push_back(d_n);
    state_n.theDouble.push_back(dg_n);
    state_n.theStensor.push_back(ep_n);

    return state_n;
}




materialState sdamageMP::getCurrentState() const
{
    materialState state_c = smallStrainMP::getCurrentState();

    state_c.theDouble.push_back(d_c);
    state_c.theDouble.push_back(dg_c);
    state_c.theStensor.push_back(ep_c);

    return state_c;
}




double sdamageMP::plasticSlip() const
{
    return ep_c.norm();
}




void sdamageMP::deviatoricStress(istensor& s) const
{
    istensor sigma;
    this->stress(sigma);
    s = istensor::deviatoricPart(sigma);
}




double sdamageMP::damage() const
{
    return d_c;
}




void sdamageMP::commitCurrentState()
{
    smallStrainMP::commitCurrentState(); // commits eps_c
    d_n  = d_c;
    dg_n = dg_c;
    ep_n = ep_c;
}




void sdamageMP::resetCurrentState()
{
    smallStrainMP::resetCurrentState();

    d_c    = d_n;
    dg_c   = dg_n;
    ep_c   = ep_n;
}




GTN_Material::GTN_Material(const std::string& name,
                                     const double xE,     const double xnu,
                                     const double xrho,   const double xq1,
                                     const double xq2, const double xY0)
:
sdamageMaterial(name, xE, xnu, xrho, xY0),
q1(xq1), q2(xq2)
{

}




GTN_Material::GTN_Material(const std::string& name,
                           const materialProperties& cl)
:
sdamageMaterial(name, cl),
q1(0.0), q2(0.0)
{
    muesli::assignValue(cl, "q1_gurson",   q1);
    muesli::assignValue(cl, "q2_gurson",   q2);
}




bool GTN_Material::check() const
{
    if (mu > 0 && lambda+2.0*mu > 0) return true;
    else return false;
}




smallStrainMP* GTN_Material::createMaterialPoint() const
{
    smallStrainMP* mp = new GTN_MP(*this);
    return mp;
}




// this function is much faster than the one with string property names, because
// avoids string comparisons. It should be used.
double GTN_Material::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_LAMBDA:     ret = lambda;   break;
        case PR_MU:         ret = mu;       break;
        case PR_YOUNG:      ret = E;        break;
        case PR_POISSON:    ret = nu;       break;
        case PR_BULK:       ret = bulk;     break;
        case PR_CP:         ret = cp;       break;
        case PR_CS:         ret = cs;       break;
        case PR_YIELD:      ret = Y0;       break;
        case PR_Q1_GURSON:  ret = q1;       break;
        case PR_Q2_GURSON:  ret = q2;       break;

        default:
            std::cout << "Error in damageMaterial. Property not defined";
    }
    return ret;
}




void GTN_Material::print(std::ostream &of) const
{
    of  << "\n Elastoplastic damaged material for small strain kinematics."
        << "\n   Damage model: Gurson-Tvergaard-Needleman yield model with no hardening."
        << "\n   Yield stress           : " << Y0
        << "\n   q1 GTN parameter       : " << q1
        << "\n   q2 GTN parameter       : " << q2
        << "\n   Young modulus:  E      : " << E
        << "\n   Poisson ratio:  nu     : " << nu
        << "\n   Lame constants: lambda : " << lambda
        << "\n   Shear modulus:  mu     : " << mu
        << "\n   Bulk modulus:   k      : " << bulk
        << "\n   Density                : " << rho;

    if (rho > 0.0)
    {
        of  << "\n   Wave velocities c_p    : " << cp;
        of  << "\n                   c_s    : " << cs;
    }
}




void GTN_Material::setRandom()
{
    E      = muesli::randomUniform(100.0, 10000.0);
    nu     = muesli::randomUniform(0.05, 0.45);
    rho    = muesli::randomUniform(1.0, 100.0);

    lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
    mu     = E/2.0/(1.0+nu);
    cp     = sqrt((lambda+2.0*mu)/rho);
    cs     = sqrt(2.0*mu/rho);
    bulk   = lambda + 2.0/3.0*mu;

    Y0     = E * 1e-3 * muesli::randomUniform(0.5, 1.5);
    q1     = muesli::randomUniform(1.0, 2.0);
    q2     = 1e-4 * muesli::randomUniform(0.5, 1.5);
}




bool GTN_Material::test(std::ostream  &of)
{
    setRandom();

    smallStrainMP* p = this->createMaterialPoint();
    p->setRandom();

    bool ok = p->testImplementation(of);
    delete p;


    return ok;
}




double GTN_Material::waveVelocity() const
{
    return cp;
}




GTN_MP::GTN_MP(const GTN_Material &m) :
muesli::sdamageMP(m),
theGTNMaterial(m)
{
    ep_n.setZero();
    ep_c.setZero();
}




void GTN_MP::updateCurrentState(const double theTime, const istensor& strain)
{
    smallStrainMP::updateCurrentState(theTime, strain);

    gursonReturn(dg_c, strain);
    eps_c  = strain;
}




double GTN_MP::vol_pslip() const
{
    istensor ep_dev_c  = istensor::deviatoricPart(ep_c);
    istensor ep_vol_c  = ep_c - ep_dev_c;

    return ep_vol_c.trace();
}




double GTN_MP::deviatoricEnergy() const
{
    istensor eps_e = istensor::deviatoricPart(eps_c - ep_c);
    return (1.0 - d_c) * theGTNMaterial.mu * eps_e.contract(eps_e);
}




void GTN_MP::deviatoricStress(istensor& s) const
{
    s = (1.0 - d_c) * 2.0 * theGTNMaterial.mu * (istensor::deviatoricPart(eps_c-ep_c));
}




double GTN_MP::energyDissipationInStep() const
{
    const double k  = theGTNMaterial.bulk;
    const double mu = theGTNMaterial.mu;

    const istensor ee_c       = istensor::deviatoricPart(eps_c-ep_c);
    istensor       ee_vol_c   = (eps_c-ep_c) - ee_c;
    istensor       stress_c   = (1.0 - d_c) * (2.0*mu*ee_c + 3.0*k* ee_vol_c);
    const double   dt_Psistar = istensor(ep_c - ep_n).dot(stress_c);

    return dt_Psistar;
}




double GTN_MP::effectiveStoredEnergy() const
{
    const double k  = theGTNMaterial.bulk;
    const double mu = theGTNMaterial.mu;

    istensor ee_c       = istensor::deviatoricPart(eps_c-ep_c);
    istensor ee_vol_c   = (eps_c-ep_c) - ee_c;
    istensor stress_c   = (1.0 - d_c) * (2.0*mu*ee_c + 3.0*k* ee_vol_c);
    double   t          = (eps_c-ep_c).trace();

    double   We         = (1.0 - d_c) * (mu*ee_c.squaredNorm() + 0.5*k*t*t);
    double   Wp         = 0.0;

    double   dt_Psistar = istensor(ep_c-ep_n).dot(stress_c);

    return We+Wp+dt_Psistar;
}




double GTN_MP::eq_stress() const
{
    const double mu        = theGTNMaterial.mu;

    istensor ee_c      = istensor::deviatoricPart(eps_c-ep_c);
    istensor stress_c  = (1.0 - d_c) * 2.0*mu*ee_c;
    return sqrt(1.5)*stress_c.norm();
}




double GTN_MP::fvFunction(const GTN_Material& m, const double& d, const double& t) // void fraction?
{
    const double q1 = m.q1;
    const double q2 = m.q2;

    double aux_cosh  = 0.5*q2*t;
    double fv        = 0.0;
    double fv_check  = (1.0-d)*(1.0-d) - 1.0 - (q1*fv)*(q1*fv) + 2.0*q1*fv*cosh(aux_cosh);
    double dfv_check = 0.0;
    size_t count     = 0;

    while (abs(fv_check) >= FVTOL && ++count < FVMAXITER)
    {
        fv_check  = (1.0-d)*(1.0-d) - 1.0 - (q1*fv)*(q1*fv) + 2.0*q1*fv*cosh(aux_cosh);
        dfv_check = 2.0*q1*cosh(aux_cosh) - 2.0*q1*q1*fv;
        fv = fv - fv_check/dfv_check;
    }

    if (count == FVMAXITER)
        std::cout << "\n   computation for fv as a function of d not converged:\n";

    return fv;
}




// Gurson mapping return: return to yield surface for Gurson-Tvergaard-Needleman model
// Non linear system of 5 equations with 5 unknowns: tr(sigma_c),||s_c||, d_c, fv_c, dg_c
void GTN_MP::gursonReturn(double &dg, const istensor& strain)
{
    const double k  = theGTNMaterial.bulk;
    const double mu = theGTNMaterial.mu;
    const double q1 = theGTNMaterial.q1;
    const double q2 = theGTNMaterial.q2;
    const double Y0 = theGTNMaterial.Y0;

    // trial state --> elastic step
/*put trial state in fcn for generality?*/
    double   dTrial     = d_n;                                            // trial value of damage variable
    double   dgTrial    = dg;
    istensor eTrial     = strain-ep_n;                                    // trial elastic strain tensor
    istensor eeTrial    = istensor::deviatoricPart(strain-ep_n);        // trial elastic deviatoric strain tensor
    istensor eVolTrial  = eTrial - eeTrial;                               // trial elastic volumetric strain tensor
    istensor sigmaTrial = (1.0-d_n) * (3.0*k*eVolTrial + 2.0*mu*eeTrial); // trial stress tensor
    istensor sTrial     = istensor::deviatoricPart(sigmaTrial);         // trial deviatoric stress tensor
    double   ssTrial    = sTrial.norm()/Y0;                               // norm of the trial deviatoric stress tensor over Y0
    double   tsTrial    = sigmaTrial.trace()/Y0;                          // trace of the trial stress tensor over Y0

    double   fvTrial    =  fvFunction(theGTNMaterial,dTrial,tsTrial);  // trial volume fraction of voids

    //check yield condition
    const double fTrial = yieldfunction(theGTNMaterial, fvTrial, ssTrial, tsTrial);

    if (fTrial <= TOL) // If elastic step is valid, save results
    {

        d_c  = d_n;
        ep_c = ep_n;
        dg   = 0.0;
    }

    else // If elastic trial is not valid, do plastic step: do return mapping
    {
        // 1. Initialize for Newton-Raphson solver --> the results of the elastic trial step.
/*put gursonSolution as parameter for generality --> same as for jacobian*/        double gursonSolution[5];  // array containing the trial value of the variables in the system of equations for Gurson model
        gursonSolution[0] = tsTrial; // trace of the trial stress tensor over Y0
        gursonSolution[1] = ssTrial; // norm of the trial deviatoric stress tensor over Y0
        gursonSolution[2] = dTrial; // trial value of damage variable
        gursonSolution[3] = fvTrial; // trial volume fraction of voids
        gursonSolution[4] = dgTrial; // trial elastic strain tensor / plastic multiplier

        realvector gursonIndependentTerm = systemEqFunction(theGTNMaterial,gursonSolution,ssTrial,tsTrial,dTrial); // value of the system of equations for Gurson model for the trial variables

        matrix gursonJacobian;
        gursonJacobian.setZero();


        // 2. Apply Newton-Raphson method
        double error = gursonIndependentTerm.norm();
        size_t count = 0;
        while (error > TOL && ++count < MAXITER)
        {
            // 2.1. Update the iteration variables
            gursonIndependentTerm = systemEqFunction(theGTNMaterial,gursonSolution,ssTrial,tsTrial,dTrial); // array containing the value of the system of equations of the Gurson model for the new computed value of the variables
            gursonJacobian = jacobianFunction(theGTNMaterial,gursonSolution,dTrial,ssTrial,tsTrial);  // Jacobian matrix of the system of equations for Gurson model


            // 2.2. Compute update step:
            // Solve A*dx=b (gursonJacobian * dx_gursonSolution = gursonIndependentTerm), overwriting b with x
            gursonJacobian.solveFull(gursonIndependentTerm); // A(x_n) * dx_n = b(x_n)

/*put as parameter for generality*/       //size_t j = gursonIndependentTerm.size(); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! put outside of loop, should be constant
            size_t j = 5;

            // 2.3. Update the state
            for (size_t k=0; k<j; k++)
            {
                gursonSolution[k] += - gursonIndependentTerm[k]; // x_n+1 = x_n + dx_n
            }

            // 2.4. Compute new error
            error = systemEqFunction(theGTNMaterial,gursonSolution,ssTrial,tsTrial,dTrial).norm();
            realvector error_vector = systemEqFunction(theGTNMaterial,gursonSolution,ssTrial,tsTrial,dTrial); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Maybe put initialization before loop to avoid reallocation of storrage else the gursonindependentterm inside the loop
        }

        if (count == MAXITER) std::cout << "\n   NR for gursonReturn not converged \n" ;

        // 3. Extract results
        istensor s_c = (gursonSolution[1]/(ssTrial))*sTrial;
        // correct internal variables
        d_c  = gursonSolution[2];
        dg   = gursonSolution[4];
/*put in fcn for generality*/       ep_c = ep_n + dg*(3.0*s_c/(Y0*Y0)+(q1*q2*gursonSolution[3]/Y0)*sinh(0.5*q2*gursonSolution[0])*istensor::identity());
    }
}




double GTN_MP::kineticPotential() const
{
    const double k  = theGTNMaterial.bulk;
    const double mu = theGTNMaterial.mu;
    double dt_Psistar = 0.0;

    const istensor ee_c     = istensor::deviatoricPart(eps_c-ep_c);
    istensor       ee_vol_c = (eps_c-ep_c) - ee_c;
    istensor       stress_c = (1.0 - d_c) * (2.0*mu*ee_c + 3.0*k* ee_vol_c);
    dt_Psistar = istensor(ep_c - ep_n).dot(stress_c);

    double dt = time_c - time_n;
    return (dt == 0.0) ? 0.0 : dt_Psistar/dt;
}




// Function which provides the Jacobian matrix of the system of equations for GTN
matrix GTN_MP::jacobianFunction (const GTN_Material& m, double *variables, const double d, const double s, const double t)
{
    const double mu = m.mu;
    const double k  = m.bulk;

    matrix matrix_jacobian(5,5);
    matrix_jacobian.setZero();

    // Function which provides the Jacobian matrix of the system of equations for GTN model
    const double q1 = m.q1;
    const double q2 = m.q2;
    const double Y0 = m.Y0;

    const double aux_A    = 1.0 + (1.0 - variables[2])*6.0*mu*variables[4]/(Y0*Y0);
    const double aux_B    = (1.0 - variables[2])/(1.0-d);
    const double aux_cosh = 0.5*q2*variables[0];

    matrix_jacobian(0,0) = 1.0 + 9.0*k*q1*q2*q2*(1.0 - variables[2])*variables[3]*variables[4]*cosh(aux_cosh)/(2.0*Y0*Y0);
    matrix_jacobian(0,1) = 0.0;
    matrix_jacobian(0,2) = t/(1.0 - d) - 9.0*k*q1*q2*variables[3]*variables[4]*sinh(aux_cosh)/(Y0*Y0);
    matrix_jacobian(0,3) = (1.0 - variables[2])*9.0*k*q1*q2*variables[4]*sinh(aux_cosh)/(Y0*Y0);
    matrix_jacobian(0,4) = (1.0 - variables[2])*9.0*k*q1*q2*variables[3]*sinh(aux_cosh)/(Y0*Y0);

    matrix_jacobian(1,0) = 0.0;
    matrix_jacobian(1,1) = 2.0*aux_A*aux_A*variables[1];
    matrix_jacobian(1,2) = -12.0*mu*aux_A*variables[1]*variables[1]*variables[4]/(Y0*Y0) + 2.0*s*s*aux_B/(1.0-d);
    matrix_jacobian(1,3) = 0.0;
    matrix_jacobian(1,4) = 12.0*mu*variables[1]*variables[1]*aux_A*(1.0 - variables[2])/(Y0*Y0);

    matrix_jacobian(2,0) = -variables[4]*9.0*k*q1*q2*q2*variables[3]*(1.0 - variables[2])*(1.0 - variables[2])*cosh(aux_cosh)/(2.0*Y0*Y0*variables[0]) + variables[4]*9.0*k*q1*q2*variables[3]*(1.0 - variables[2])*(1.0 - variables[2])*sinh(aux_cosh)/(Y0*Y0*variables[0]*variables[0]);
    matrix_jacobian(2,1) = 0.0;
    matrix_jacobian(2,2) = 1.0 + variables[4]*18.0*k*q1*q2*variables[3]*(1.0 - variables[2])*sinh(aux_cosh)/(Y0*Y0*variables[0]);
    matrix_jacobian(2,3) = -variables[4]*9.0*k*q1*q2*(1.0 - variables[2])*(1.0 - variables[2])*sinh(aux_cosh)/(Y0*Y0*variables[0]);
    matrix_jacobian(2,4) = -variables[3]*9.0*k*q1*q2*(1.0 - variables[2])*(1.0 - variables[2])*sinh(aux_cosh)/(Y0*Y0*variables[0]);

    matrix_jacobian(3,0) = q1*q2*variables[3]*sinh(aux_cosh);
    matrix_jacobian(3,1) = 3.0*variables[1];
    matrix_jacobian(3,2) = 0.0;
    matrix_jacobian(3,3) = 2.0*q1*cosh(aux_cosh) - 2.0*q1*q1*variables[3];
    matrix_jacobian(3,4) = 0.0;

    matrix_jacobian(4,0) = q1*q2*variables[3]*sinh(aux_cosh);
    matrix_jacobian(4,1) = 0.0;
    matrix_jacobian(4,2) = -2.0*(1.0 - variables[2]);
    matrix_jacobian(4,3) = -2.0*q1*q1*variables[3] + 2.0*q1*cosh(aux_cosh);
    matrix_jacobian(4,4) = 0.0;

    return matrix_jacobian;
}




void GTN_MP::setRandom()
{
    smallStrainMP::setRandom();

    istensor tmp;
    tmp.setRandom();

    ep_n = tmp;
    ep_c = ep_n;

    d_n  = muesli::randomUniform(0.0, 0.5);
    d_c  = d_n;
}




double GTN_MP::shearStiffness() const
{
    return theGTNMaterial.mu;
}



double GTN_MP::storedEnergy() const
{
    const double k  = theGTNMaterial.bulk;
    const double mu = theGTNMaterial.mu;

    double th = eps_c.trace() - ep_c.trace();
    double We = (1.0-d_c)*(mu*(istensor::deviatoricPart(eps_c-ep_c)).squaredNorm() + 0.5*k*th*th);
    double Wp = 0.0;

    return We+Wp;
}




void GTN_MP::stress(istensor& sigma) const
{
    deviatoricStress(sigma);
    sigma += (1.0 - d_c)*theGTNMaterial.bulk*(eps_c-ep_c).trace()*istensor::identity();
}




// Function which provides the system of equations for the damage model
realvector GTN_MP::systemEqFunction(const GTN_Material& m, double *variables, const double s, const double t, const double d)
{
    const double mu = m.mu;
    const double k  = m.bulk;

    // Function which provides the system of equations for Gurson model
    const double q1 = m.q1;
    const double q2 = m.q2;
    const double Y0 = m.Y0;

    const double   aux_A    = 1.0 + (1.0 - variables[2])*6.0*mu*variables[4]/(Y0*Y0);
    const double   aux_B    = (1.0 - variables[2])/(1.0-d);
    const double   aux_cosh = 0.5*q2*variables[0];

    realvector     system_eq(size_t(5));

    system_eq[0] = variables[0] - (1.0-variables[2])*t/(1.0-d) + (1.0-variables[2])*variables[4]*9.0*k*q1*q2*variables[3]*sinh(aux_cosh)/(Y0*Y0);
    system_eq[1] = variables[1]*variables[1]*aux_A*aux_A - aux_B*aux_B*s*s;
    system_eq[2] = variables[2] - d - variables[4]*variables[3]*9.0*k*q1*q2*(1.0 - variables[2])*(1.0 - variables[2])*sinh(aux_cosh)/(Y0*Y0*variables[0]);
    system_eq[3] = 1.5*variables[1]*variables[1] + 2.0*q1*variables[3]*cosh(aux_cosh) - 1.0 - q1*q1*variables[3]*variables[3];
    system_eq[4] = (1.0 - variables[2])*(1.0 - variables[2]) - 1.0 - q1*q1*variables[3]*variables[3] + 2.0*q1*variables[3]*cosh(aux_cosh);

    return system_eq;
}




thPotentials GTN_MP::thermodynamicPotentials() const
{
    thPotentials tp;
    return tp;
}




void GTN_MP::tangentTensor(itensor4& C) const
{
    C.setZero();

    const double   lambda      = theGTNMaterial.lambda;
    const double   mu          = theGTNMaterial.mu;

    // STEP 1: build the fourth order elastic stiffness tensor
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    if (i==j && k==l) C(i,j,k,l) += lambda;
                    if (i==k && j==l) C(i,j,k,l) += mu;
                    if (i==l && j==k) C(i,j,k,l) += mu;

       if (dg_c == 0.0)
        {
            // STEP 2: build the fourth order damaged elastic stiffness tensor
            C = (1.0-d_n)*C;
        }


       else
        {
            // STEP 2: build the fourth order damaged elastic stiffness tensor
            // PENDING TO BE CORRECTED (e.andres 20171024)
            C = (1.0-d_n)*C;
        }
    }
}




double GTN_MP::void_fraction() const
{
    const double k        = theGTNMaterial.bulk;
    const double Y0       = theGTNMaterial.Y0;

    double   d        = d_c;
    istensor ee_dev_c = istensor::deviatoricPart(eps_c-ep_c);
    istensor ee_vol_c = (eps_c-ep_c) - ee_dev_c;
    double   t        = ((1.0 - d_c) * (3.0*k*ee_vol_c)).trace()/Y0;

    return fvFunction(theGTNMaterial,d,t);

}




double GTN_MP::vol_stress() const
{
    const double k             = theGTNMaterial.bulk;

    istensor ee_c          = istensor::deviatoricPart(eps_c-ep_c);
    istensor ee_vol_c      = (eps_c-ep_c) - ee_c;
    istensor stress_vol_c  = (1.0 - d_c) * 3.0*k*ee_vol_c;

    return stress_vol_c.trace();
}




double GTN_MP::volumetricEnergy() const
{
    double th = (eps_c-ep_c).trace();
    double V  = (1.0 - d_c) * 0.5 * theGTNMaterial.bulk * th * th;

    return V;
}




double GTN_MP::yieldfunction(const GTN_Material& m, const double& fv, const double& s, const double& t)
{
    const double q1       = m.q1;
    const double q2       = m.q2;

    const double aux_cosh = 0.5*q2*t;

    return 1.5*s*s + 2.0*q1*fv*cosh(aux_cosh) - 1.0 - (q1*fv)*(q1*fv);
}




Gurson_Material::Gurson_Material(const std::string& name,
                                     const double xE, const double xnu,
                                     const double xrho, const double xR_inf,
                                     const double xR_b, const double xY0)
:
sdamageMaterial(name, xE, xnu, xrho, xY0),
R_inf(xR_inf), R_b(xR_b)
{

}




Gurson_Material::Gurson_Material(const std::string& name,
                           const materialProperties& cl)
:
sdamageMaterial(name, cl),
R_inf(0.0), R_b(0.0)
{
    muesli::assignValue(cl, "rinf_hardening", R_inf);
    muesli::assignValue(cl, "rb_hardening", R_b);

}




bool Gurson_Material::check() const
{
    if (mu > 0 && lambda+2.0*mu > 0) return true;
    else return false;
}




smallStrainMP* Gurson_Material::createMaterialPoint() const
{
    smallStrainMP* mp = new Gurson_MP(*this);
    return mp;
}




// this function is much faster than the one with string property names, because
// avoids string comparisons. It should be used.
double Gurson_Material::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_LAMBDA:     ret = lambda;   break;
        case PR_MU:         ret = mu;       break;
        case PR_YOUNG:      ret = E;        break;
        case PR_POISSON:    ret = nu;       break;
        case PR_BULK:       ret = bulk;     break;
        case PR_CP:         ret = cp;       break;
        case PR_CS:         ret = cs;       break;
        case PR_YIELD:      ret = Y0;       break;
        case PR_Rinf_Hardening: ret = R_inf;break;
        case PR_Rb_Hardening: ret = R_b;    break;

        default:
            std::cout << "Error in damageMaterial. Property not defined";
    }
    return ret;
}




void Gurson_Material::print(std::ostream &of) const
{
    of  << "\n Elastoplastic damaged material for small strain kinematics."
        << "\n   Damage model: Gurson-Tvergaard-Needleman yield model with no hardening."
        << "\n   Yield stress           : " << Y0
        << "\n   Hardening convergence parameter: " << R_inf
        << "\n   Hardening rate parameter       : " << R_b
        << "\n   Young modulus:  E      : " << E
        << "\n   Poisson ratio:  nu     : " << nu
        << "\n   Lame constants: lambda : " << lambda
        << "\n   Shear modulus:  mu     : " << mu
        << "\n   Bulk modulus:   k      : " << bulk
        << "\n   Density                : " << rho;

    if (rho > 0.0)
    {
        of  << "\n   Wave velocities c_p    : " << cp;
        of  << "\n                   c_s    : " << cs;
    }
}




void Gurson_Material::setRandom()
{
    E      = muesli::randomUniform(100.0, 10000.0);
    nu     = muesli::randomUniform(0.05, 0.45);
    rho    = muesli::randomUniform(1.0, 100.0);

    lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
    mu     = E/2.0/(1.0+nu);
    cp     = sqrt((lambda+2.0*mu)/rho);
    cs     = sqrt(2.0*mu/rho);
    bulk   = lambda + 2.0/3.0*mu;

    Y0     = muesli::randomUniform(0.05, 0.10) * E;
    R_inf  = muesli::randomUniform(2.0, 3.0) * Y0;
    R_b    = muesli::randomUniform(0.1, 0.8);
}




bool Gurson_Material::test(std::ostream  &of)
{
    setRandom();

    smallStrainMP* p = this->createMaterialPoint();
    p->setRandom();

    bool ok = p->testImplementation(of);
    delete p;


    return ok;
}




Gurson_MP::Gurson_MP(const Gurson_Material &m) :
muesli::sdamageMP(m),
theGursonMaterial(m),
R_n(0.0), R_c(0.0),
Jac_c(4,4)
{
    d_n = 0.07;
    d_c = 0.07;
}




double Gurson_MP::a_fcn(const double& d_f, const double& p_f, const double& sig_yf)
{
    return 1.0 + d_f*d_f - 2.0*d_f*cosh(3.0*p_f/(2.0*sig_yf));
}




double Gurson_MP::b_fcn(const double& p_f, const double& sig_yf)
{
    return sinh(3.0*p_f/(2.0*sig_yf));
}




double Gurson_MP::deviatoricEnergy() const
{
    istensor eps_e = istensor::deviatoricPart(eps_c - ep_c);
    return theGursonMaterial.mu * eps_e.contract(eps_e);
}




void Gurson_MP::deviatoricStress(istensor& s) const
{
    s = 2.0 * theGursonMaterial.mu * (istensor::deviatoricPart(eps_c-ep_c));
}




void Gurson_MP::dres_num(const double p_f, const double R_f, const double D_f, const double delgam_f, const double& evetrial, const double& J2ede, matrix& dres_f)
{
    realvector Res_nump(4);
    realvector Res_numm(4);

    const double d_x = 1e-12;

    dg_c = delgam_f + d_x;
    residual(p_f, evetrial, J2ede, Res_nump);
    dg_c = delgam_f - d_x;
    residual(p_f, evetrial, J2ede, Res_numm);
    dg_c = delgam_f;
    Res_nump = (Res_nump-Res_numm)/(2.0*d_x) ;
    dres_f(0,0) =Res_nump[0];
    dres_f(1,0) =Res_nump[1];
    dres_f(2,0) =Res_nump[2];
    dres_f(3,0) =Res_nump[3];

    residual(p_f+d_x, evetrial, J2ede, Res_nump);
    residual(p_f-d_x, evetrial, J2ede, Res_numm);
    Res_nump = (Res_nump-Res_numm)/(2.0*d_x) ;
    dres_f(0,1) =Res_nump[0];
    dres_f(1,1) =Res_nump[1];
    dres_f(2,1) =Res_nump[2];
    dres_f(3,1) =Res_nump[3];

    d_c = D_f + d_x;
    residual(p_f, evetrial, J2ede, Res_nump);
    d_c = D_f - d_x;
    residual(p_f, evetrial, J2ede, Res_numm);
    d_c = D_f;
    Res_nump = (Res_nump-Res_numm)/(2.0*d_x) ;
    dres_f(0,2) =Res_nump[0];
    dres_f(1,2) =Res_nump[1];
    dres_f(2,2) =Res_nump[2];
    dres_f(3,2) =Res_nump[3];

    R_c = R_f + d_x;
    residual(p_f, evetrial, J2ede, Res_nump);
    R_c = R_f - d_x;
    residual(p_f, evetrial, J2ede, Res_numm);
    R_c = R_f;
    Res_nump = (Res_nump-Res_numm)/(2.0*d_x) ;
    dres_f(0,3) =Res_nump[0];
    dres_f(1,3) =Res_nump[1];
    dres_f(2,3) =Res_nump[2];
    dres_f(3,3) =Res_nump[3];
}




void Gurson_MP::dresidual(const double &pc, const double &evetrial, const double &J2ede, matrix& dres_f)
{
    const double& G          = theGursonMaterial.mu;
    const double& kap        = theGursonMaterial.bulk;
    const double sig_yc     = yieldstress(R_c);
    const double sig_yc_sq  = sig_yc*sig_yc;
    const double dsig_yc    = dyieldstress(R_c);
    const double a_f        = a_fcn(d_c, pc, sig_yc);
    const double b_f        = b_fcn(pc, sig_yc);

    // da
    const double dadp = -d_c*sinh(3.0*pc/(2.0*sig_yc))*3.0/sig_yc;
    const double dadR = d_c*sinh(3.0*pc/(2.0*sig_yc))*3.0*pc/sig_yc_sq*dsig_yc;
    const double dadd = 2.0*d_c -2.0*cosh(3.0*pc/(2.0*sig_yc));

    // db
    const double dbdp = cosh(3.0*pc/(2.0*sig_yc))*3.0/(2.0*sig_yc);
    const double dbdR = -cosh(3.0*pc/(2.0*sig_yc))*3.0*pc/(2.0*sig_yc_sq)*dsig_yc;

    //dphi
    dres_f(0,0) = -2.0*pow(2.0*G/(1.0+2.0*G*dg_c), 3)*J2ede; // ddgam
    dres_f(0,1) = -1.0/3.0*sig_yc_sq*dadp; // dpc
    dres_f(0,2) = -1.0/3.0*sig_yc_sq*dadd; // ddc
    dres_f(0,3) = -1.0/3.0*sig_yc_sq*dadR -2.0/3.0*a_f*sig_yc*dsig_yc; // drc

    //dpc
    dres_f(1,0) = kap*b_f*sig_yc;
    dres_f(1,1) = 1.0+dg_c*kap*sig_yc*dbdp;
    dres_f(1,2) = 0.0;
    dres_f(1,3) = dg_c*b_f*kap*dsig_yc+dg_c*kap*sig_yc*dbdR;

    //dd
    double dhelp = d_c-d_c*d_c;
    dres_f(2,0) = -b_f*dhelp*sig_yc;
    dres_f(2,1) = -dg_c*dhelp*sig_yc*dbdp;
    dres_f(2,2) = 1.0-dg_c*b_f*sig_yc*(1.0-2.0*d_c);
    dres_f(2,3) = -dg_c*dhelp*(b_f*dsig_yc+sig_yc*dbdR);

    //dR
    dres_f(3,0) = -1.0/(1.0-d_c)*(2.0/3.0*a_f*sig_yc+b_f*pc*d_c);
    dres_f(3,1) = -dg_c/(1.0-d_c)*(2.0/3.0*sig_yc*dadp +b_f*d_c+pc*d_c*dbdp);
    dres_f(3,2) = -dg_c/pow(1-d_c,2)*(2.0/3.0*a_f*sig_yc+b_f*pc*d_c) -dg_c/(1.0-d_c)*(2.0/3.0*sig_yc*dadd+pc*b_f);
    dres_f(3,3) = 1.0-dg_c/(1.0-d_c)*(2.0/3.0*(dadR*sig_yc+a_f*dsig_yc)+ pc*d_c*dbdR);
}




double Gurson_MP::dyieldstress(const double &R_f)
{
    return  theGursonMaterial.R_inf * theGursonMaterial.R_b * exp(-theGursonMaterial.R_b * R_f);
}




double Gurson_MP::effectiveStoredEnergy() const
{
    return storedEnergy() + energyDissipationInStep();
}




double Gurson_MP::energyDissipationInStep() const
{
    // plastic strain contribution
    istensor s_c;
    deviatoricStress(s_c);
    double dissipation = (ep_c - ep_n).dot(s_c);

    // hardening contribution
    double kap = theGursonMaterial.R_inf * (1.0 - exp(-theGursonMaterial.R_b * R_c));
    dissipation -= (R_c-R_n)*kap;

    return dissipation;
}




double Gurson_MP::kineticPotential() const
{
    cout << "Kinetic Potential not defined" << endl;
    //  For Lemaitre Material Model according to definition in MUESLI
    return 0;
}




void Gurson_MP::residual(const double &pc, const double &evetrial, const double &J2ede, realvector &res_f)
{
    const double& G      = theGursonMaterial.mu;
    const double& kap    = theGursonMaterial.bulk;
    const double sig_yc = yieldstress(R_c);
    const double a_f    = a_fcn(d_c, pc, sig_yc);
    const double b_f    = b_fcn(pc, sig_yc);

    res_f[0] = pow(2.0*G/(1.0+2.0*G*dg_c),2)*J2ede-1.0/3.0*a_f*sig_yc*sig_yc;
    res_f[1] = pc -kap*evetrial +dg_c*kap*b_f*sig_yc;
    res_f[2] = d_c -d_n -dg_c*b_f*(d_c-d_c*d_c)*sig_yc;
    res_f[3] = R_c -R_n -dg_c/(1.0-d_c)*(2.0/3.0*a_f*sig_yc+b_f*pc*d_c);
}




void Gurson_MP::setRandom()
{
    smallStrainMP::setRandom();

    istensor tmp;
    tmp.setRandom();
    ep_n = tmp;
    ep_c = ep_n;

    d_n  = muesli::randomUniform(0.001, 0.5);
    d_c  = d_n;

    R_n  = muesli::randomUniform(0.0, 0.5);
    R_c  = R_n;
}




double Gurson_MP::shearStiffness() const
{
    return theGursonMaterial.mu;
}




double Gurson_MP::storedEnergy() const
{
    // stored in strain
    double We = deviatoricEnergy() + volumetricEnergy();

    // stored in hardening
    const double& R_inf = theGursonMaterial.R_inf;
    const double& R_b   = theGursonMaterial.R_b;
    double Wp = R_inf * (R_c + 1.0/R_b * exp(-R_b*R_c) - 1.0/R_b);

    return We+Wp;
}




void Gurson_MP::stress(istensor& sigma) const
{
    deviatoricStress(sigma);
    sigma += theGursonMaterial.bulk*(eps_c-ep_c).trace()*istensor::identity();
}




void Gurson_MP::tangentTensor(itensor4& C) const // 12.3.5 in Computational Methods for Plasticity by Souza Neto
{
    C.setZero();
    const double& lambda = theGursonMaterial.lambda;
    const double& G      = theGursonMaterial.mu;
    const double& K      = theGursonMaterial.bulk;

    if (dg_c == 0.0) // elastic step --> elastic stiffness matrix
    {
        istensor I   = istensor::identity();
        itensor4 I_I = itensor4::dyadic(I, I);

        C = 2.0*G*itensor4::identitySymm() + lambda * I_I;
    }

    else // plastic step --> plastic stiffness matrix
    {
        istensor eedTrial     = istensor::deviatoricPart(eps_c-ep_n);
        istensor I            = istensor::identity();
        itensor4 I_d          = itensor4::deviatoricIdentity();
        const double g_help   = 2.0*G/(1.0+2.0*G*dg_c);
        const double gsq_help = g_help*g_help;
        const double& C_11    = Jac_c(0,0);
        const double& C_12    = Jac_c(0,1);
        const double& C_21    = Jac_c(1,0);
        const double& C_22    = Jac_c(1,1);

        C = g_help*I_d
        +gsq_help*itensor4::dyadic(eedTrial, C_11*gsq_help*eedTrial-C_12*K*I)
        -         itensor4::dyadic(I,        C_21*gsq_help*eedTrial-C_22*K*I);
    }

}




thPotentials Gurson_MP::thermodynamicPotentials() const
{
    thPotentials tp;
    return tp;
}




void Gurson_MP::updateCurrentState(const double theTime, const istensor& strain) // Box 12.6 in Computational Methods for Plasticity by Souza Neto
{
    eps_c  = strain;

    const double& k  = theGursonMaterial.bulk;
    const double& mu = theGursonMaterial.mu;

    // trial state --> elastic step
    istensor eeTrial    = strain-ep_n;
    istensor eedTrial   = istensor::deviatoricPart(eeTrial);
    istensor eeVTrial  = eeTrial - eedTrial;
    istensor sigmaTrial = (3.0*k*eeVTrial + 2.0*mu*eedTrial);
    istensor sTrial     = istensor::deviatoricPart(sigmaTrial);
    double   pTrial     = sigmaTrial.trace()/3.0; // 6.88 in Computational Methods for plasticity
    double   YTrial     = yieldstress(R_n);

    // yield criterion
    double phiTrial = sTrial.J2()-1.0/3.0*a_fcn(d_n,pTrial,YTrial)*YTrial*YTrial;
    if (phiTrial <= TOL) // if elastic step is valid, save results
    {
        //cout << "elastic" << endl;
        R_c = R_n;
        d_c  = d_n;
        ep_c = ep_n;
        dg_c = 0.0;
    }

    else // do plastic step: do return mapping
    {
        const double evetrial_f = eeTrial.trace();
        const istensor edetrail_f = eps_c - ep_n - istensor::identity() * evetrial_f/3.0;
        const double J2edetrial_f = edetrail_f.J2();

        matrix jac_f(4,4);
        realvector Res_f(4);
        double pc = pTrial;

        int niter = 0;
        while (niter<MAXITER) {
            niter ++;

            residual(pc, evetrial_f, J2edetrial_f, Res_f);
            double totres = abs(Res_f[0])+abs(Res_f[1])+abs(Res_f[2])+abs(Res_f[3]);
            if (totres<TOL || niter == MAXITER) // update variables and save state
            {
                //cout << "plastic: phi = " << phiTrial << " Number of Iterations: " << niter << endl;
                if (d_c > 1.0)
                {
                    std::cout << "Error-Gurson: MP failed - damage" << std::endl;
                    d_c = 1.0;
                }
                else if (niter == MAXITER){ std::cout << "Error-Gurson: Algorithm not converged! " << std::endl;}

                istensor ee = 1.0/(1.0+2.0*mu*dg_c)*edetrail_f + pc/(3.0*k)*istensor::identity(); // !!!!!!!!!!!!!!!!!! p/3 !!!
                ep_c = eps_c - ee;

                jac_f.invert();
                Jac_c = jac_f;

                break;
            }

            dresidual(pc, evetrial_f, J2edetrial_f, jac_f);
            jac_f.solveFull(Res_f);
            dg_c = dg_c - Res_f[0];
            pc = pc - Res_f[1];
            d_c = d_c - Res_f[2];
            R_c = R_c - Res_f[3];
        }
    }
}




double Gurson_MP::volumetricEnergy() const
{
    double th = (eps_c-ep_c).trace();
    double V  = 0.5 * theGursonMaterial.bulk * th * th;

    return V;
}




double Gurson_MP::yieldstress(const double &R_f)
{
    return theGursonMaterial.Y0 + theGursonMaterial.R_inf * (1.0 - exp(-theGursonMaterial.R_b * R_f));
}





// Lemaitre-Chaboche isotropic damge material model without hardening
Lemaitre_Material::Lemaitre_Material(const std::string& name,
                                     const materialProperties& cl) :
muesli::sdamageMaterial(name, cl),
r(0.0), s(0.0), R_inf(0.0), R_b(0.0)
{
    muesli::assignValue(cl, "r_lemaitre", r);
    muesli::assignValue(cl, "s_lemaitre", s);
    muesli::assignValue(cl, "rinf_hardening", R_inf);
    muesli::assignValue(cl, "rb_hardening", R_b);
}




Lemaitre_Material::Lemaitre_Material(const std::string& name,
                                     const double xE, const double xnu,
                                     const double xrho, const double xr,
                                     const double xs, const double xY0,
                                     const double xR_inf, const double xR_b) :
sdamageMaterial(name, xE, xnu, xrho, xY0),
r(xr), s(xs), R_inf(xR_inf), R_b(xR_b)
{
}




bool Lemaitre_Material::check() const
{
    if (mu > 0.0 && lambda+2.0*mu > 0.0) return true;
    else return false;
}




muesli::smallStrainMP* Lemaitre_Material::createMaterialPoint() const
{
    smallStrainMP* mp = new Lemaitre_MP(*this);
    return mp;
}




double Lemaitre_Material::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_LAMBDA:     ret = lambda;   break;
        case PR_MU:         ret = mu;       break;
        case PR_YOUNG:      ret = E;        break;
        case PR_POISSON:    ret = nu;       break;
        case PR_BULK:       ret = bulk;     break;
        case PR_CP:         ret = cp;       break;
        case PR_CS:         ret = cs;       break;
        case PR_YIELD:      ret = Y0;       break;
        case PR_R_Lemaitre:  ret = r;       break;
        case PR_S_Lemaitre:  ret = s;       break;
        case PR_Rinf_Hardening:  ret = R_inf;break;
        case PR_Rb_Hardening:  ret = R_b;    break;

        default:
            std::cout << "Error in damageMaterial. Property not defined";
    }
    return ret;
}




void Lemaitre_Material::print(std::ostream &of) const
{

    of  << "\n Elastoplastic damaged material for small strain kinematics."
        << "\n   Damage model: Lemaitre-Chaboche damage model with no kinematic hardening."
        << "\n   Yield stress                       : " << Y0
        << "\n   r Lemaitre parameter (Dividing)    : " << r
        << "\n   s Lemaitre parameter (Exponent)    : " << s
        << "\n   Hardening convergence parameter    : " << R_inf
        << "\n   Hardening rate parameter           : " << R_b
        << "\n   Young modulus:  E                  : " << E
        << "\n   Poisson ratio:  nu                 : " << nu
        << "\n   Lame constants: lambda             : " << lambda
        << "\n   Shear modulus:  mu                 : " << mu
        << "\n   Bulk modulus:   k                  : " << bulk
        << "\n   Density                            : " << rho;

    if (rho > 0.0)
    {
        of  << "\n   Wave velocities c_p    : " << cp;
        of  << "\n                   c_s    : " << cs;
    }
}




void Lemaitre_Material::setRandom()
{
    E      = muesli::randomUniform(1000.0, 10000.0);
    nu     = muesli::randomUniform(0.05, 0.45);
    rho    = muesli::randomUniform(1.0, 100.0);

    lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
    mu     = E/2.0/(1.0+nu);
    cp     = sqrt((lambda+2.0*mu)/rho);
    cs     = sqrt(2.0*mu/rho);
    bulk   = lambda + 2.0/3.0*mu;

    Y0     = muesli::randomUniform(0.05, 0.10) * E;
    R_inf  = muesli::randomUniform(2.0, 3.0) * Y0;
    R_b    = muesli::randomUniform(0.1, 0.8);

    r      = muesli::randomUniform(0.01, 0.02) * R_inf;
    s      = muesli::randomUniform(1.0, 2.0);
}




bool Lemaitre_Material::test(std::ostream  &of)
{
    setRandom();

    smallStrainMP* p = this->createMaterialPoint();
    p->setRandom();

    bool ok = p->testImplementation(of, false, true);
    delete p;

    return ok;
}




Lemaitre_MP::Lemaitre_MP(const Lemaitre_Material &m) :
    muesli::sdamageMP(m), // sets also damage material to 0
    theLemaitreMaterial(m),
    R_n(0.0), R_c(0.0)
{
}




void Lemaitre_MP::CheckUpdateState() const // According to chapter 12.4 in Computational Methods for Plasticity by Souza de Neto
{
    // Error in evolutionary equation for damage
    double eev = (eps_c-ep_n).trace(); // Volumetric strain
    double p_tilaux = theLemaitreMaterial.bulk*eev * eev/(2.0); // Undamaged volumetric stress K*eev as helper variable
    double sigy = yieldstress(R_c); // New iterative yield stress
    double Y = -sigy*sigy/(6.0*theLemaitreMaterial.mu) - p_tilaux; // Damage energy release rate
    double Err_d = d_c-d_n - (dg_c/(1-d_c)*pow(-Y/theLemaitreMaterial.r,theLemaitreMaterial.s));

    // Error in evolutionary equation for hardening
    double Err_R = R_c-R_n -(dg_c);

    // Error in evolutionary equation for plastic flow
    istensor ee_c = eps_c - ep_c;
    istensor edec = ee_c - ee_c.trace()/3.0*istensor::identity();
    istensor snp1 = (1.0-d_c)*2.0*theLemaitreMaterial.mu*edec;
    double Err_ep = (ee_c - ( eps_c - ep_n - dg_c*SQRT3_2*snp1/( (1.0-d_c)*snp1.norm()) )).norm();

    // Error in yield criterion
    double Err_phi = yieldfunction();

    // Check if all evolutionary equations are fulfilled:
    if (abs(Err_d) > 1e-6) {std::cout << "Check Damage Evolution: "<< Err_d << std::endl;}// Check damage evolution
    if (abs(Err_R) > 1e-6) {std::cout << "Check Hardening Evolution: " << Err_R << std::endl;} // Check hardening evolution
    if (abs(Err_ep) > 1e-6) {std::cout << "Check Plastic Flow: " << Err_ep << std::endl;} // Check plastic flow
    if (abs(Err_phi) > 1e-6) {std::cout << "Check Yieldcrit: " << Err_phi << std::endl;} // Check yield criterion
}




void Lemaitre_MP::commitCurrentState()
{
    sdamageMP::commitCurrentState(); // commits eps_c, ep_c, d_c, dg_c, R_c
    R_n = R_c;
}




double Lemaitre_MP::deviatoricEnergy() const
{
    return (1.0 - d_c) * theLemaitreMaterial.mu * (istensor::deviatoricPart(eps_c - ep_c)).squaredNorm();
}




void Lemaitre_MP::deviatoricStress(istensor& s) const
{
    s = (1.0-d_c) * 2.0 * theLemaitreMaterial.mu * istensor::deviatoricPart(eps_c-ep_c);
}




double Lemaitre_MP::dyieldstress(const double R_f) const // derivative of yield stress due to isotropic hardening
{
    return theLemaitreMaterial.R_inf * theLemaitreMaterial.R_b * exp(-theLemaitreMaterial.R_b * R_f);
}




double Lemaitre_MP::effectiveStoredEnergy() const
{
    return storedEnergy() + energyDissipationInStep();
}




double Lemaitre_MP::energyDissipationInStep() const
{
    // plastic strain contribution
    istensor s_c;
    deviatoricStress(s_c);
    double dissipation = (ep_c - ep_n).dot(s_c);

    // hardening contribution
    double kap = theLemaitreMaterial.R_inf * (1.0 - exp(-theLemaitreMaterial.R_b * R_c));
    dissipation += (R_c-R_n)*kap;

    // damage contribution
    double Y_c = -(deviatoricEnergy() + volumetricEnergy())/(1.0-d_c) ;
    dissipation -= (d_c-d_n)*Y_c;

    return dissipation;
}




double Lemaitre_MP::kineticPotential() const
{
    cout << "Kinetic Potential not defined" << endl;
    //  For Lemaitre Material Model according to definition in MUESLI
    return 0;
}




void Lemaitre_MP::setRandom()
{
    time_n = muesli::randomUniform(0.01, 0.1);
    R_n = 0.0;
    R_c = R_n;

    d_n  = 0.0;
    d_c = d_n;

    istensor ee;
    ee.setRandom();
    ee -= istensor::scaledIdentity(ee.trace()/3.0);
    ee *= 1.0/ee.norm();
    ee *= theLemaitreMaterial.Y0 / sqrt(1.5) / 2.0 / theLemaitreMaterial.mu * 0.99;
    eps_n = ee;

    eps_c = eps_n;
    ep_n.setZero();
    ep_c = ep_n;
    time_c = time_n + muesli::randomUniform(0.01, 0.1);
}




double Lemaitre_MP::shearStiffness() const
{
    return theLemaitreMaterial.mu;
}




double Lemaitre_MP::storedEnergy() const
{
    double We = deviatoricEnergy() + volumetricEnergy();

    const double& R_inf = theLemaitreMaterial.R_inf;
    const double& R_b   = theLemaitreMaterial.R_b;
    double Wp = R_inf * (R_c + 1.0/R_b * exp(-R_b*R_c) - 1.0/R_b);

    return We + Wp;
}




void Lemaitre_MP::stress(istensor& sigma) const
{
    const double& k = theLemaitreMaterial.bulk;

    deviatoricStress(sigma);
    double th = (eps_c-ep_c).trace();
    sigma += istensor::scaledIdentity((1.0-d_c) * k * th);
}




void Lemaitre_MP::tangentTensor(itensor4& C) const // According to chapter 12.4 in Computational Methods for Plasticity by Souza de Neto
{
    const double& lambda = theLemaitreMaterial.lambda;
    const double& G = theLemaitreMaterial.mu;
    const double& K = theLemaitreMaterial.bulk;
    const double& r = theLemaitreMaterial.r;
    const double& s = theLemaitreMaterial.s;


    if (dg_c != 0.0)
    { // elasto-plastic tangent tensor
        // extract variables
        double w_c = 1.0 - d_c;

        istensor sigma; stress(sigma);
        istensor epse_tr = eps_c - ep_n;
        istensor ee_tr = istensor::deviatoricPart(epse_tr);
        double q_tr = SQRT3_2*2.0*G*ee_tr.norm();
        double w_n = 1.0 - d_n;
        double qtil_tr = q_tr/w_n;
        double ptil = sigma.trace()/w_c/3.0;

        istensor ss; deviatoricStress(ss);
        istensor ss_bar = ss/ss.norm();

        double sigy = yieldstress(R_c);
        double Dsigy = dyieldstress(R_c);

        double Y = -sigy*sigy/(6.0*G) -ptil*ptil/(2.0*K);
        double DY = -Dsigy*sigy / (3.0*G);

        double dw = (3.0*G + w_c*Dsigy)/(qtil_tr-sigy);
        double DRes = dw - Dsigy/(3.0*G)*pow(-Y/r,s)
            - s*DY*(qtil_tr-sigy)/(3.0*G*r)*pow(-Y/r, s-1.0);

        double a1 = ( w_c/(qtil_tr-sigy) -1.0/(3.0*G)*pow(-Y/r,s) )/DRes;
        double a2 = -s*ptil*(qtil_tr-sigy)/(3.0*G*r*K*DRes) * pow(-Y/r,s-1.0);
        double a3 = dw*a2;
        double a4 = a1*dw - w_c/(qtil_tr-sigy);

        double a = 2.0*G*w_c*sigy/qtil_tr;
        double b = 2.0*G*(a1*Dsigy*w_c +a4*sigy -sigy*w_c/qtil_tr);
        double c = K*sqrt(2.0/3.0)*(a2*Dsigy*w_c + a3 *sigy);
        double d = 2.0*G*SQRT3_2*ptil*a4;
        double e = K*(w_c+a3*ptil);

        istensor I = istensor::identity();
        itensor4 I_I = itensor4::dyadic(I, I);
        itensor4 I_D = itensor4::deviatoricIdentity();
        itensor4 s_I = itensor4::dyadic(ss_bar, I);
        itensor4 s_s = itensor4::dyadic(ss_bar, ss_bar);
        itensor4 I_s = itensor4::dyadic(I, ss_bar);

        C = a * I_D + b * s_s + c * s_I + d * I_s + e * I_I;
    }
    else
    { // elastic tangent tensor

        istensor I = istensor::identity();
        itensor4 I_I = itensor4::dyadic(I, I);
        C = (1.0-d_c)*(2.0*G*itensor4::identitySymm() + lambda * I_I);
    }
}




thPotentials Lemaitre_MP::thermodynamicPotentials() const
{
    thPotentials tp;
    return tp;
}




// bookkeeping
void Lemaitre_MP::updateCurrentState(const double theTime, const istensor& strain) // According to box 12.4 in Computational Methods for Plasticity by Souza de Neto
{
    const double& K = theLemaitreMaterial.bulk;
    const double& G = theLemaitreMaterial.mu;

    eps_c = strain;
    double eev = (eps_c-ep_n).trace(); // Volumetric strain
    istensor eed_til = eps_c - ep_n - istensor::identity() * eev/3.0; // Elastic trial deviatoric strain
    double q_til = SQRT3_2*(2.0*G*eed_til).norm(); // Undamaged trial equivalent stress
    double phi_tr = q_til - yieldstress(R_n); // Trial yield criterion

    if (phi_tr <= 0.0) // Elastic load case
    {
        R_c = R_n;
        d_c = d_n;
        dg_c = 0.0;
        ep_c = ep_n;
    }
    else // Plastic load case
    {
        const double& r = theLemaitreMaterial.r; // Denominator of Y --> (-Y/r)
        const double& s = theLemaitreMaterial.s; // Exponent of (-Y/r) --> (-Y/r)^s
        double w_n = 1.0 - d_n; // Old integrity
        double p_tilaux = K*eev * eev/(2.0); // Undamaged volumetric stress K*eev as helper variable

        dg_c = w_n*phi_tr/(3.0*G); // Initial guess
        R_c = R_n + dg_c; // Inital guess of hardening variable

        int niter = 0;
        while (niter<MAXITER) {
            niter ++;

            double sigy = yieldstress(R_c); // New iterative yield stress
            double fyield = q_til-sigy; // Yield criterion using new sigy0
            double w_c = 3.0*G/fyield*dg_c; // Damage variable
            double Y = -sigy*sigy/(6.0*G) - p_tilaux; // Damage energy release rate
            double Res = w_c - w_n + fyield/(3.0*G) * pow(-Y/r,s); // Residual

            if (abs(Res)<TOL || niter == MAXITER) // update variables and save state
            {
                d_c = 1.0-w_c;
                if (d_c > 1.0)
                {
                    std::cout << "Error-Lemaitre: MP failed - damage" << std::endl;
                    d_c = 1.0;
                }
                else if (niter == MAXITER){ std::cout << "Error-Lemaitre: Algorithm not converged! " << std::endl;}

                istensor nn; nn = eed_til; nn *= 1.0/nn.norm(); // Compute flow direction
                ep_c = ep_n + dg_c * sqrt(1.5)/w_c * nn; // Compute plastic flow
                break;
            }
            double dsigy = dyieldstress(R_c);
            double DY = -dsigy*sigy/(3.0*G);
            double DRes = 3.0*G/fyield + w_c*dsigy/fyield - dsigy/(3.0*G)*pow(-Y/r, s) - s*DY/(3.0*G/fyield*r)*pow(-Y/r, s-1); // Compute derivative of residual
            dg_c += -Res/DRes;  R_c = R_n + dg_c; // Update states using Newton-Raphson
        }
    }
    //CheckUpdateState();
}




double Lemaitre_MP::volumetricEnergy() const
{
    double th = (eps_c-ep_c).trace();
    return (1.0 - d_c) * 0.5 * theLemaitreMaterial.bulk * th * th;
}




double Lemaitre_MP::yieldfunction() const
{
    istensor sig = (1.0-d_c)*2.0*theLemaitreMaterial.mu*istensor::deviatoricPart(eps_c-ep_c);
    double sig_mises = SQRT3_2*sig.norm();
    double sigy_c = yieldstress(R_c);

    return sig_mises/(1.0-d_c) - sigy_c;
}




double Lemaitre_MP::yieldstress(const double R_f) const
{
    return theLemaitreMaterial.Y0 + theLemaitreMaterial.R_inf * (1.0 - exp(-theLemaitreMaterial.R_b * R_f));
}





// Lemaitre-Chaboche isotropic damge material model WITH kinematic hardening as defined in Computational Methods for Plasticity by Souza Neto - 2008 in Chapter 12.3
LemKin_Material::LemKin_Material(const std::string& name,
                                     const materialProperties& cl) :
muesli::sdamageMaterial(name, cl),
r(0.0), s(0.0), R_inf(0.0), R_b(0.0), a(0.0), b(0.0), C_v(6,6), C_vinv(6,6)
{
    muesli::assignValue(cl, "r_lemaitre", r);
    muesli::assignValue(cl, "s_lemaitre", s);
    muesli::assignValue(cl, "rinf_hardening", R_inf);
    muesli::assignValue(cl, "rb_hardening", R_b);
    muesli::assignValue(cl, "akin_hardening", a);
    muesli::assignValue(cl, "bkin_hardening", b);

    double C_help[] = { lambda+2.0*mu,lambda,lambda, 0.0,0.0,0.0,
                        lambda,lambda+2.0*mu,lambda, 0.0,0.0,0.0,
                        lambda,lambda,lambda+2.0*mu, 0.0,0.0,0.0,
                        0.0,0.0,0.0, mu,0.0,0.0,
                        0.0,0.0,0.0, 0.0,mu,0.0,
                        0.0,0.0,0.0, 0.0,0.0,mu};
    matrix C_helpm(6,6,C_help);
    C_v = C_helpm;

    if ( matrix(C_v - matrix(6,6)).norm() < 1e-6 )
    {
        cout << "Test Mode-LemKin: Inverse of elasticity matrix not initialized" << endl;
    }
    else
    {
        C_vinv = C_v;
        C_vinv.invert();
    }
}




LemKin_Material::LemKin_Material(const std::string& name,
                                     const double xE, const double xnu,
                                     const double xrho, const double xr,
                                     const double xs, const double xY0,
                                     const double xR_inf, const double xR_b,
                                     const double xa, const double xb) :
sdamageMaterial(name, xE, xnu, xrho, xY0),
r(xr), s(xs), R_inf(xR_inf), R_b(xR_b), a(xa), b(xb), C_v(6,6), C_vinv(6,6)
{
    double C_help[] = { lambda+2.0*mu,lambda,lambda, 0.0,0.0,0.0,
                                lambda,lambda+2.0*mu,lambda, 0.0,0.0,0.0,
                                lambda,lambda,lambda+2.0*mu, 0.0,0.0,0.0,
                                0.0,0.0,0.0, mu,0.0,0.0,
                                0.0,0.0,0.0, 0.0,mu,0.0,
                                0.0,0.0,0.0, 0.0,0.0,mu};
    matrix C_helpm(6,6,C_help);
    C_v = C_helpm;

    C_vinv = C_v;
    C_vinv.invert();
}




bool LemKin_Material::check() const
{
    if (mu > 0.0 && lambda+2.0*mu > 0.0) return true;
    else return false;
}




muesli::smallStrainMP* LemKin_Material::createMaterialPoint() const
{
    smallStrainMP* mp = new LemKin_MP(*this);
    return mp;
}




double LemKin_Material::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_LAMBDA:         ret = lambda;   break;
        case PR_MU:             ret = mu;       break;
        case PR_YOUNG:          ret = E;        break;
        case PR_POISSON:        ret = nu;       break;
        case PR_BULK:           ret = bulk;     break;
        case PR_CP:             ret = cp;       break;
        case PR_CS:             ret = cs;       break;
        case PR_YIELD:          ret = Y0;       break;
        case PR_R_Lemaitre:     ret = r;        break;
        case PR_S_Lemaitre:     ret = s;        break;
        case PR_Rinf_Hardening: ret = R_inf;    break;
        case PR_Rb_Hardening:   ret = R_b;      break;
        case PR_akin_Hardening: ret = R_inf;    break;
        case PR_bkin_Hardening: ret = R_b;      break;

        default:
            std::cout << "Error in damageMaterial. Property not defined";
    }
    return ret;
}




void LemKin_Material::print(std::ostream &of) const
{

    of  << "\n Elastoplastic damaged material for small strain kinematics."
        << "\n   Damage model: Lemaitre-Chaboche damage model with kinematic hardening."
        << "\n   Yield stress                       : " << Y0
        << "\n   r Lemaitre parameter (Dividing)    : " << r
        << "\n   s Lemaitre parameter (Exponent)    : " << s
        << "\n   Hardening convergence parameter    : " << R_inf
        << "\n   Hardening rate parameter           : " << R_b
        << "\n   Kinematic hardening - a            : " << a
        << "\n   Kinematic hardening - b            : " << b
        << "\n   Young modulus:  E                  : " << E
        << "\n   Poisson ratio:  nu                 : " << nu
        << "\n   Lame constants: lambda             : " << lambda
        << "\n   Shear modulus:  mu                 : " << mu
        << "\n   Bulk modulus:   k                  : " << bulk
        << "\n   Density                            : " << rho;

    if (rho > 0.0)
    {
        of  << "\n   Wave velocities c_p    : " << cp;
        of  << "\n                   c_s    : " << cs;
    }
}




void LemKin_Material::setRandom()
{
    E      = muesli::randomUniform(1000.0, 10000.0);
    nu     = muesli::randomUniform(0.05, 0.45);
    rho    = muesli::randomUniform(1.0, 100.0);

    lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
    mu     = E/2.0/(1.0+nu);
    cp     = sqrt((lambda+2.0*mu)/rho);
    cs     = sqrt(2.0*mu/rho);
    bulk   = lambda + 2.0/3.0*mu;

    Y0     = muesli::randomUniform(0.05, 0.10) * E;
    R_inf  = muesli::randomUniform(2.0, 3.0) * Y0;
    R_b    = muesli::randomUniform(0.1, 0.8);

    a      = muesli::randomUniform(0.01, 0.08);
    b      = muesli::randomUniform(0.01, 0.08);

    r      = muesli::randomUniform(0.1, 0.2) * R_inf;
    s      = muesli::randomUniform(1.0, 2.0);

    double C_help[] = { lambda+2.0*mu,lambda,lambda, 0.0,0.0,0.0,
                        lambda,lambda+2.0*mu,lambda, 0.0,0.0,0.0,
                        lambda,lambda,lambda+2.0*mu, 0.0,0.0,0.0,
                        0.0,0.0,0.0, mu,0.0,0.0,
                        0.0,0.0,0.0, 0.0,mu,0.0,
                        0.0,0.0,0.0, 0.0,0.0,mu};
    matrix C_helpm(6,6,C_help);
    C_v = C_helpm;

    C_vinv = C_v;
    C_vinv.invert();
}




bool LemKin_Material::test(std::ostream  &of)
{
    setRandom();

    smallStrainMP* p = this->createMaterialPoint();
    p->setRandom();

    bool ok = p->testImplementation(of, false, true);
    delete p;

    return ok;
}




LemKin_MP::LemKin_MP(const LemKin_Material &m) :
    muesli::sdamageMP(m), // sets also damage in material to 0
    theLemKinMaterial(m),
    R_n(0.0), R_c(0.0),
    beta_n(0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
    beta_c(0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
    C12(6,6)
{
}




void LemKin_MP::CheckUpdateState() const // According to chapter 12.4 in Computational Methods for Plasticity by Souza de Neto
{
    const double& G = theLemKinMaterial.mu;
    const istensor epsetrial2    = eps_c-ep_c;
    const istensor eedTrial2   = istensor::deviatoricPart(epsetrial2);
    const istensor sTrial2     = (1.0-d_c)*2.0*G*eedTrial2;
    const double   YTrial2     = yieldstress(R_c);

    // Check if yield criterion is correct after plastic flow
    const double phiTrial2 = sqrt(3.0*(sTrial2-beta_c).J2())/(1.0-d_c)-YTrial2;
    cout << " Phi2: " << phiTrial2 << endl;
}




void LemKin_MP::commitCurrentState()
{
    sdamageMP::commitCurrentState(); // commits eps_c, ep_c, d_c, dg_c, R_c, beta_c
    R_n = R_c;
    beta_n = beta_c;
    dg_n = dg_c;
}




double LemKin_MP::deviatoricEnergy() const
{
    return (1.0 - d_c) * theLemKinMaterial.mu * (istensor::deviatoricPart(eps_c - ep_c)).squaredNorm();
}




void LemKin_MP::deviatoricStress(istensor& s) const
{
    s = (1.0-d_c) * 2.0 * theLemKinMaterial.mu * istensor::deviatoricPart(eps_c-ep_c);
}




void LemKin_MP::dresidual(matrix& jacf, realvector& sigf, realvector& betaf, realvector& epsetrialf) const
// Derived from the model in box 12.2 by differntiating tensors and then transforming it to voigt notation
{
    const double&   r = theLemKinMaterial.r; // Denominator of Y --> (-Y/r)
    const double&   s = theLemKinMaterial.s; // Exponent of (-Y/r) --> (-Y/r)^s
    const double&   a = theLemKinMaterial.a; // a of kinematic hardening law
    const double&   b = theLemKinMaterial.b; // b of kinematic hardening law
    const matrix&   C_v = theLemKinMaterial.C_v;
    const matrix&   C_vinv = theLemKinMaterial.C_vinv;

    const realvector    sf = II_dev2*sigf;
    const double        J23 =3.0* 1.0/2.0* (sf-betaf).dot(II_mul2*(sf-betaf));
    const double        sqrtJ23 = sqrt(J23);
    const realvector    N_f = 3.0/2.0*(sf-betaf)/((1.0-d_c)*sqrtJ23);

    const realvector    amod_v = -1.5*pow(J23, -1.5) *(sf-II_dev2*betaf);
    const realvector    bmod_v = -1.5*pow(J23, -1.5) *(betaf-sf);
    const matrix        dNdsig = 1.5/((1-d_c)*sqrtJ23)*(II_s2-1.0/3.0*IxI_2) +1.5/(1-d_c)*matrix(6, 6, amod_v, sf-betaf);
    const matrix        dNdbet = -1.5/((1-d_c)*sqrtJ23)*II_s2 +1.5/(1-d_c)*matrix(6, 6, bmod_v, sf-betaf);

    // Derivative of evolutionary equation of yield criterion
    const realvector    dphidsig = 1.5/((1-d_c)*sqrtJ23) *(sf-II_dev2*betaf);
    const realvector    dphidbet = 1.5/((1.0-d_c)*sqrtJ23)*(-sf+betaf);
    const double        dphidD   = 1.0/pow(1.0-d_c,2)*sqrtJ23;
    const double        dphiddga = -dyieldstress(R_n+dg_c);

    // Derivative of evolutionary equation of plastic strain
    const matrix        deppdsig = II_s2 + (1.0-d_c)*dg_c*II_mul2*(C_v*dNdsig);
    const matrix        deppdbet = (1.0-d_c)*dg_c*II_mul2*(C_v*dNdbet);
    const realvector    deppdD   = C_v*epsetrialf;
    const realvector    deppddga = (1.0-d_c)*II_mul2*(C_v*N_f);

    // Derivative of evolutionary equation of kinematic hardening
    const matrix        dbetdsig = -dg_c*a*dNdsig;
    const matrix        dbetdbet = II_s2-dg_c*(a*dNdbet-b*II_s2);
    const realvector    dbetdD   = -dg_c*a*N_f/(1.0-d_c);
    const realvector    dbetddga = b*betaf-a*N_f;

    // Derivative of evolutionary equation of damage
    const double        Y = 1.0/(2.0*pow(1.0-d_c,2))*sigf.dot(C_vinv*sigf);
    const realvector    dYdsig = 1.0/(2.0*pow(1.0-d_c,2))*C_vinv*sigf;
    const realvector    dDdsig = dg_c/(1.0-d_c)*s/r*pow(Y/r,s-1.0)*dYdsig;
    //const realvector    dDdbet(6,0,0,0,0,0,0);
    const double        dDdD   = 1.0-dg_c*(2.0*s+1.0)/pow(1.0-d_c,2)*pow(Y/r,s);
    const double        dDddga = -1.0/(1.0-d_c)*pow(Y/r,s);

    // load results into jacobian matrix
    for (int i(0); i<6; i++)
    {
        for (int j(0); j<6; j++)
        {
            jacf(i+1,j)     = deppdsig(i,j);
            jacf(i+1,j+8)   = deppdbet(i,j);

            jacf(i+7,j)     = dbetdsig(i,j);
            jacf(i+7,j+8)   = dbetdbet(i,j);
        }
        jacf(0,i)       = dphidsig[i];
        jacf(0,i+8)     = dphidbet[i];

        jacf(i+1,6)    = deppdD[i];
        jacf(i+1,7)    = deppddga[i];

        jacf(i+7,6)    = dbetdD[i];
        jacf(i+7,7)    = dbetddga[i];

        jacf(13,i)      = dDdsig[i];
    }
    jacf(0,6) = dphidD;
    jacf(0,7) = dphiddga;
    jacf(13,6) = dDdD;
    jacf(13,7) = dDddga;

    for (int i(0); i<14; i++)
    {
        for (int j(0); j<3; j++)
        {
            jacf(i,j+3)     = 2*jacf(i,j+3);
            jacf(i,j+11)   = 2*jacf(i,j+11);
        }
    }
}




double LemKin_MP::dyieldstress(const double R_f) const // derivative of yield stress due to isotropic hardening
{
    return theLemKinMaterial.R_inf * theLemKinMaterial.R_b * exp(-theLemKinMaterial.R_b * R_f);
}




double LemKin_MP::effectiveStoredEnergy() const
{
    return storedEnergy() + energyDissipationInStep();
}




double LemKin_MP::energyDissipationInStep() const
{
    // Plastic strain contribution
    istensor s_c;
    deviatoricStress(s_c);
    double dissipation = (ep_c - ep_n).dot(s_c);

    // Isotropic hardening contribution
    double kap = theLemKinMaterial.R_inf * (1.0 - exp(-theLemKinMaterial.R_b * R_c));
    dissipation -= (R_c-R_n)*kap;

    // Kinematic hardening contribution
    dissipation -= (beta_c/theLemKinMaterial.a).dot(beta_c-beta_n);

    // Damage contribution
    double Y_c = -(deviatoricEnergy() + volumetricEnergy())/(1.0-d_c) ;
    dissipation -= (d_c-d_n)*Y_c;

    return dissipation;
}




double LemKin_MP::kineticPotential() const
{
    cout << "Kinetic Potential not defined" << endl;
    //  For Lemaitre Material Model according to definition in MUESLI
    return 0;
}




void LemKin_MP::reframe_24(itensor4& t4_f, const matrix& t2_f) const
{ // Switching a second order tensor in voigt notation to a fourth order tensor
    const int di1[6] = {0,1,2,1,0,0};
    const int di2[6] = {0,1,2,2,2,1};
    for (int i(0); i<6; i++)
    {
        for (int j(0); j<6; j++)
        {
            int I_d1 = di1[i];  int I_d2 = di2[i]; // translating first index
            int J_d1 = di1[j];  int J_d2 = di2[j]; // translating second index

            t4_f(I_d1,I_d2, J_d1,J_d2) = t2_f(i,j);
            t4_f(I_d1,I_d2, J_d2,J_d1) = t2_f(i,j);
            t4_f(I_d2,I_d1, J_d1,J_d2) = t2_f(i,j);
            t4_f(I_d2,I_d1, J_d2,J_d1) = t2_f(i,j);
        }
    }
}




void LemKin_MP::residual(realvector& resf, realvector& sigf, realvector& betaf, realvector& betanf, realvector& epsetrialf) const
{ // Implemented the evolutionary equations as residual according to box 12.2 in Computational Methods for Plasticity
    const realvector sf = II_dev2*sigf;
    const double sqrtJ23 = sqrt(3.0* 1.0/2.0* (sf-betaf).dot(II_mul2*(sf-betaf)));
    const realvector N_f = 3.0/2.0*(sf-betaf)/((1.0-d_c)*sqrtJ23);

    resf[0] = sqrtJ23/(1.0-d_c)-yieldstress(R_n + dg_c);

    const realvector dep = sigf-(1-d_c)*theLemKinMaterial.C_v*(epsetrialf-dg_c*II_mul2*N_f);

    const realvector dbet = betaf-betanf-dg_c*(theLemKinMaterial.a*N_f-theLemKinMaterial.b*betaf);
    for (int i(1);i<7;i++)
    {
        resf[i] = dep[i-1]; // transfer residual in plastic flow
        resf[i+6] = dbet[i-1]; // transfer residual in kinematic hardening
    }

    const double sighf = (sigf[0] + sigf[1] + sigf[2])/3.0;
    const double sigeqf = sqrt(1.5*sf.dot(II_mul2*sf));
    const double Rvf = 2.0/3.0*(1+theLemKinMaterial.nu)+3.0*(1.0-2.0*theLemKinMaterial.nu)*pow(sighf/sigeqf,2);
    const double Yc = pow(sigeqf/(1.0-d_c),2)*Rvf/(2.0*theLemKinMaterial.E);
    resf[13] = d_c - d_n - dg_c/(1-d_c)*pow(Yc/theLemKinMaterial.r,theLemKinMaterial.s);
}




void LemKin_MP::setRandom()
{
    time_n = muesli::randomUniform(0.01, 0.1);

    R_n = 0.0;
    R_c = R_n;

    d_n  = 0.0;
    d_c = d_n;

    istensor ee;
    ee.setRandom();
    ee -= istensor::scaledIdentity(ee.trace()/3.0);
    ee *= 1.0/ee.norm();
    ee *= theLemKinMaterial.Y0 / sqrt(1.5) / 2.0 / theLemKinMaterial.mu * 0.99;
    eps_n = ee;

    eps_c = eps_n;
    ep_n.setZero();
    ep_c = ep_n;
    time_c = time_n + muesli::randomUniform(0.01, 0.1);
}




double LemKin_MP::shearStiffness() const
{
    return theLemKinMaterial.mu;
}



double LemKin_MP::storedEnergy() const
{
    double We = deviatoricEnergy() + volumetricEnergy();

    const double& R_inf = theLemKinMaterial.R_inf;
    const double& R_b   = theLemKinMaterial.R_b;
    double Wp = R_inf * (R_c + 1.0/R_b * exp(-R_b*R_c) - 1.0/R_b);

    return We + Wp;
}




void LemKin_MP::stress(istensor& sigma) const
{
    const double& k = theLemKinMaterial.bulk;

    deviatoricStress(sigma);
    double th = (eps_c-ep_c).trace();
    sigma += istensor::scaledIdentity((1.0-d_c) * k * th);
}




void LemKin_MP::tangentTensor(itensor4& C) const // According to chapter 12.3.3 in Computational Methods for Plasticity by Souza de Neto
{
    C.setZero();
    const double& lambda = theLemKinMaterial.lambda;
    const double& G = theLemKinMaterial.mu;

    if (dg_c != 0.0)
    { // elasto-plastic tangent tensor
        reframe_24(C, C12*theLemKinMaterial.C_v);
        C = (1-d_c)*C;

    }
    else
    { // elastic tangent tensor
        istensor I = istensor::identity();
        itensor4 I_I = itensor4::dyadic(I, I);
        C = (1.0-d_c)*(2.0*G*itensor4::identitySymm() + lambda * I_I);
    }
}




thPotentials LemKin_MP::thermodynamicPotentials() const
{
    thPotentials tp;
    return tp;
}




// bookkeeping
void LemKin_MP::updateCurrentState(const double theTime, const istensor& strain) // According to box 12.3.2 in Computational Methods for Plasticity by Souza de Neto
{
    const double& K = theLemKinMaterial.bulk;
    const double& G = theLemKinMaterial.mu;

    const istensor epsetrial    = strain-ep_n;
    const istensor eedTrial   = istensor::deviatoricPart(epsetrial);
    const istensor sTrial     = (1.0-d_n)*2.0*G*eedTrial;
    const double   YTrial     = yieldstress(R_n);

    // yield criterion
    const double phiTrial = sqrt(3.0*(sTrial-beta_n).J2())/(1.0-d_n)-YTrial;
    if (phiTrial <= 0.0) // Elastic load case
    {
        //cout << "elastic" << endl;
        ep_c = ep_n;
        beta_c = beta_n;
        d_c = d_n;
        R_c = R_n;
        dg_c = 0.0;
        eps_c = strain;
    }
    else // Plastic load case
    {
        const istensor eeVTrial  = epsetrial - eedTrial;
        const istensor sigmaTrial = (1-d_n)*(3.0*K*eeVTrial + 2.0*G*eedTrial);

        // Define variables for iteration in voigt notation
        realvector sig_v(6), beta_v(6), beta_nv(6), epsetr_v(6); // in Voigt-Notation
        matrix jac_m(14,14); // the gradient for Newton-Raphson
        voigt_21(sigmaTrial, sig_v);
        voigt_21(beta_n, beta_v);
        voigt_21(beta_n, beta_nv);
        voigt_21eps(epsetrial, epsetr_v);
        realvector res_v(14);
        res_v.setZero();

        // Initialize variables
        d_c = d_n;
        dg_c = 0.0;

        int niter = 0;
        while (niter<MAXITER) {
            niter ++;

            residual(res_v, sig_v, beta_v, beta_nv, epsetr_v); // Compute residual
            double Res = res_v.norm(); // Compute the scalar error

            // Check for convergence. If so: post-process: Update variables and save state
            if (Res<TOL || niter == MAXITER)
            {
                if (d_c > 1.0) // Check for fully damaged material point
                {
                    std::cout << "Error-LemKin: MP failed - damage" << std::endl;
                    d_c = 0.9999999;
                }
                else if (niter == MAXITER) // Check if the algorithm did not converge
                {
                    std::cout << "Error-LemKin: Algorithm not converged! Res: " << Res << std::endl;
                }
                //cout << "i: " << niter << " d_c: " << d_c << " Res: " << res_v[0] <<  endl;
                //cout << "Residuals: flow: " << abs(res_v[1])+abs(res_v[2])+abs(res_v[3])+abs(res_v[4])+abs(res_v[5])+abs(res_v[6]) << " beta: " << abs(res_v[7])+abs(res_v[8])+abs(res_v[9])+abs(res_v[10])+abs(res_v[11])+abs(res_v[12]) << " Damage: " << res_v[12] << endl; // Compute the error for each evolutionary equation

                // Update the state variables
                const realvector sf = II_dev2*sig_v;
                const double sqrtJ23 = sqrt(3.0* 1.0/2.0* (sf-beta_v).dot(II_mul2*(sf-beta_v)));
                const realvector N_f = 3.0/2.0*(sf-beta_v)/((1.0-d_c)*sqrtJ23);
                istensor N_m;
                voigt_12eps(N_m, II_mul2*N_f);

                ep_c = ep_n + dg_c*N_m ; // Update plastic strain
                voigt_12(beta_c, beta_v); // Update kinematic hardening
                R_c = R_n + dg_c;  // Update isotropic hardening
                eps_c = strain; // Update the external strain measure

                // Save the matrix for the tangent modulus
                jac_m.invert();
                for (int i(0); i<6; i++)
                {
                    for (int j(0); j<6; j++)
                    {
                        C12(i,j) = jac_m(i,j+1);
                    }
                }

                break;
            }
            dresidual(jac_m, sig_v, beta_v, epsetr_v); // Compute derivative of residual

            jac_m.solveFull(res_v); // Solve Newton-Raphson step

            // Update states
            for (int i(0); i<6; i++)
            {
                sig_v[i] -= res_v[i];
                beta_v[i] -= res_v[i+8];
            }
            d_c -= res_v[6];
            dg_c -= res_v[7];

        }
    }
}





void LemKin_MP::voigt_12(istensor& mat, const realvector& vec) const
{ // Transform a second order tensor into a vector in voigt notation
    mat(0,0) = vec[0];
    mat(1,1) = vec[1];
    mat(2,2) = vec[2];
    mat(1,2) = vec[3];
    mat(0,2) = vec[4];
    mat(0,1) = vec[5];
}




void LemKin_MP::voigt_21(const istensor& mat, realvector& vec) const
{ // Transform a second order tensor into a vector in voigt notation
    vec[0] = mat(0,0);
    vec[1] = mat(1,1);
    vec[2] = mat(2,2);
    vec[3] = mat(1,2);
    vec[4] = mat(0,2);
    vec[5] = mat(0,1);
}




void LemKin_MP::voigt_12eps(istensor& mat, const realvector& vec) const
{ // Transform a second order tensor into a vector in voigt notation for strains
    mat(0,0) = vec[0];
    mat(1,1) = vec[1];
    mat(2,2) = vec[2];
    mat(1,2) = 0.5*vec[3];
    mat(0,2) = 0.5*vec[4];
    mat(0,1) = 0.5*vec[5];
}




void LemKin_MP::voigt_21eps(const istensor& mat, realvector& vec) const
{ // Transform a second order tensor into a vector in voigt notation for strains
    vec[0] = mat(0,0);
    vec[1] = mat(1,1);
    vec[2] = mat(2,2);
    vec[3] = 2.0*mat(1,2);
    vec[4] = 2.0*mat(0,2);
    vec[5] = 2.0*mat(0,1);
}




double LemKin_MP::volumetricEnergy() const
{
    double th = (eps_c-ep_c).trace();
    return (1.0 - d_c) * 0.5 * theLemKinMaterial.bulk * th * th;
}




double LemKin_MP::yieldfunction() const
{
    istensor sig = (1.0-d_c)*2.0*theLemKinMaterial.mu*istensor::deviatoricPart(eps_c-ep_c);
    double sig_mises = SQRT3_2*sig.norm();
    double sigy_c = yieldstress(R_c);

    return sig_mises/(1.0-d_c) - sigy_c;
}




double LemKin_MP::yieldstress(const double R_f) const
{
    return theLemKinMaterial.Y0 + theLemKinMaterial.R_inf * (1.0 - exp(-theLemKinMaterial.R_b * R_f));
}
